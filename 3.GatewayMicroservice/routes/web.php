<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * Routes for Actors
 */
$router->post('/acteur/chercher', 'ActorController@findActor');
$router->post('/acteur/oauth/token', 'ActorController@actorToken');


$router->get('/acteurs', 'ActorController@index');
$router->post('/acteur/ajouter', 'ActorController@store');
$router->get('/acteur/details/{id}', 'ActorController@show');
$router->put('/acteur/modifier/{id}', 'ActorController@update');
$router->patch('/acteur/modifier/{id}', 'ActorController@update');
$router->delete('/acteur/supprimer/{id}', 'ActorController@destroy');


/**
 * Routes for Movies
 */
$router->get('/films', 'MovieController@index');
$router->post('/film/ajouter', 'MovieController@store');
$router->get('/film/details/{id}', 'MovieController@show');
$router->put('/film/modifier/{id}', 'MovieController@update');
$router->patch('/film/modifier/{id}', 'MovieController@update');
$router->delete('/film/supprimer/{id}', 'MovieController@destroy');

$router->get('/films/{year}', 'MovieController@movieByYear');
$router->get('/acteur/{id}/films', 'MovieController@movieByActorId');
$router->post('/film/oauth/token', 'MovieController@movieToken');





$router->post('/unserialize', 'MovieController@unserialize');

 
// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });
