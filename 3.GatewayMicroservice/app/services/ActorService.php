<?php

namespace App\services;

use App\Models\Microservice;
use App\Traits\ConsumesExternalService;

class ActorService
{
    use ConsumesExternalService;


    /**
     * baseUri the base uri to consume the actors service
     *
     * @var string
     */
    public $baseUri;

    /**
     *  the secret uri to consume the actors service
     *
     * @var string
     */
    // public $secret;

    public function __construct()
    {
        $this->baseUri = Microservice::find(1)->base_url;
    }


    public function obtainClientId($data)
    {

        /**
         * Get client_id with the name of client
         */
        return $this->performRequest('POST', '/actors/token', $data);
    }
    public function obtainActorToken($data)
    {
        /**
         * Get Token Actor for access microservice
         */
        return $this->performRequest('POST', '/oauth/token', $data);
    }


    /**
     * Obtain the full List of actor from the actor service
     *
     * @return string
     */
    public function obtainActors()
    {

        return $this->performRequest('GET', '/actors/all');
    }
    /**
     * Obtain 1 actor from actor service  
     * @return string
     */
    public function obtainActor($id)
    {

        return $this->performRequest('GET', "/actor/read/{$id}");
    }
    public function searchActor($data)
    {

        // dd('serialize');
        // $dataSeri = serialize($data);
        // return $dataSeri;


        return $this->performRequest('POST', '/actor/find', $data);
    }
    public function searchInternalActor($data, $headers)
    {
        // dd('serialize');
        // $dataSeri = serialize($data);
        // return $dataSeri;


        return $this->performInternalRequest('POST', '/actor/find', $data, $headers);
    }



    /**
     *Create one Actor usning Actor service
     *
     * @return string
     */
    public function createActor($data)
    {
        return $this->performRequest('POST', '/actor/create', $data);
    }




    /**
     * Update a actor from actor service
     *
     * @param $data $data [explicite description]
     *
     * @return void
     */
    public function updateActor($data, $id)
    {

        return $this->performRequest('PUT', "/actor/update/${id}", $data);
    }

    /**
     * delete a Actor from Actor service
   
     * @return string
     */
    public function deleteActor($id)
    {
        return $this->performRequest('DELETE', "/actor/delete/${id}");
    }
}
