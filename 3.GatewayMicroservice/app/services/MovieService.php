<?php

namespace App\services;

use App\Models\Microservice;
use App\Traits\ConsumesExternalService;

class MovieService
{
    use ConsumesExternalService;


    /**
     * baseUri the base uri to consume the movies service
     *
     * @var string
     */
    public $baseUri;

    /**
     *  the secret uri to consume the movies service
     *
     * @var string
     */
    public $secret;

    public function __construct()
    {
        // $this->baseUri = config('services.movies.base_uri');
        // $this->secret = config('services.movies.secret');

        $this->baseUri = Microservice::find(2)->base_url;

        // $this->secret = config('services.movies.secret');
    }


    public function obtainClientId($data)
    {

        /**
         * Get client_id with the name of client
         */
        return $this->performRequest('POST', '/movies/token', $data);
    }
    public function obtainMovieToken($data)
    {
        /**
         * Get Token Movie for access microservice
         */
        return $this->performRequest('POST', '/oauth/token', $data);
    }

    /**
     * Obtain the full List of movie from the movie service
     *
     * @return string
     */
    public function obtainMovies()
    {

        return $this->performRequest('GET', '/movies/all');
    }
    /**
     * Obtain 1 movie from movie service  
     * @return string
     */
    public function obtainMovie($id)
    {

        return $this->performRequest('GET', "/movie/read/{$id}");
    }
    public function searchMovie($year)
    {

        return $this->performRequest('GET', "/movie/{$year}");
    }

    public function searchMovieByActorId($id)
    {
       
        return $this->performRequest('GET', "/movie/actor/{$id}");
    }





    /**
     *Create one Movie usning Movie service
     *
     * @return string
     */
    public function createMovie($data)
    {
        return $this->performRequest('POST', '/movie/create', $data);
    }

    /**
     * Update a movie from movie service
     *
     * @param $data $data [explicite description]
     *
     * @return void
     */
    public function updateMovie($data, $id)
    {

        return $this->performRequest('PUT', "/movie/update/${id}", $data);
    }

    /**
     * delete a Movie from Movie service
   
     * @return string
     */
    public function deleteMovie($id)
    {
        return $this->performRequest('DELETE', "/movie/delete/${id}");
    }
}
