<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponser
{
    /**
     * Method Build success response from service(Actor or movies)
     * @return json
     */
    public function successResponse($data, $code = Response::HTTP_OK)
    {
        // return $data['data'];
        return response($data, $code)->header('content-type', 'application/json');
    }


    /**
     * Method Build error response from service(Actor or movies)
     * @return json
     */
    public function errorMessage($message, $code)
    {
        return response($message, $code)->header('content-type', 'application/json');
    }

    /**
     * Method Build error response from service gateway  
     * @return json
     */
    public function errorResponse($message, $code)
    {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }
}
