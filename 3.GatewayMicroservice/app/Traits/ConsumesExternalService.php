<?php

namespace App\Traits;

use GuzzleHttp\client;

trait ConsumesExternalService
{
    /**
     * send a request to any service
     * @return string
     */
    public function performRequest($method, $requestUrl, $formParams = [], $headers = [])
    {
        //Prepare Authorization for request microservices
        // dd($requestUrl);

        // dd($requestUrl);
        $autorization = app('request')->header('Authorization');
        if (isset($autorization)) {
            // dd($headers);

            $headers['Authorization'] = $autorization;
        }


        $client = new client([
            'base_uri' => $this->baseUri,
        ]);


        $response = $client->request($method, $requestUrl, ['form_params' => $formParams, 'headers' => $headers]);

        return json_decode($response->getBody(), true);

        // return $response->getBody()->getContents();
        // return json_decode($response->getBody()->getContents(), true);



    }
    public function performInternalRequest($method, $requestUrl, $formParams = [], $headers = [])
    {

        // dd($headers);
        // dd($headers);
        // return $headers;
        // return "test";
        //Prepare Authorization for request microservices
        // $autorization = app('request')->header('Authorization');
        // if (isset($autorization)) {
        //     // dd($headers);

        //     $headers['Authorization'] = $autorization;
        // }


        $client = new client([
            'base_uri' => $this->baseUri,
        ]);
        // dd($this->baseUri);
        // dd($method);

        $response = $client->request($method, $requestUrl, ['form_params' => $formParams, 'headers' => $headers]);
        // return $response;
        // dd(json_decode($response->getBody(), true));
        return json_decode($response->getBody(), true);

        // return $response->getBody()->getContents();
        // return json_decode($response->getBody()->getContents(), true);



    }
}
