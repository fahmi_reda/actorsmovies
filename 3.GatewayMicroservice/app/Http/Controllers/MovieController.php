<?php

namespace App\Http\Controllers;

// use App\Models\Ac;
// use App\services\MovieService;

use App\services\ActorService;
use App\services\MovieService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\client;


class MovieController extends Controller
{
    use ApiResponser;

    /**
     * The service to consume the movies microservice
     *
     * @var MovieService
     */
    public $movieService;
    /**
     * The service to consume the Actors microservice
     *
     * @var ActorService
     */
    public $actorService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(MovieService $movieService, ActorService $actorService)
    {
        $this->movieService = $movieService;
        $this->actorService = $actorService;
    }

    public function movieToken(Request $request)
    {
        //Request to Actors microservice to get client_id with the name given
        $response = $this->successResponse($this->movieService->obtainClientId($request->all(), Response::HTTP_CREATED));
        $responseJsonDecode = json_decode($response->getContent(), true);

        $data = [
            'grant_type' => 'client_credentials',
            'client_id' => $responseJsonDecode['data'],
            'client_secret' => base64_encode($request->client_secret),
        ];

        return  $this->successResponse($this->movieService->obtainMovieToken($data));
    }
    public function index()
    {
        return  $this->successResponse($this->movieService->obtainMovies());
    }

    public function show($id)
    {
        return $this->successResponse($this->movieService->obtainMovie($id));
    }
    public function movieByYear($year)
    {

        return $this->successResponse($this->movieService->searchMovie($year));
    }
    public function movieByActorId($id)
    {

        return $this->successResponse($this->movieService->searchMovieByActorId($id));
    }

    public function store(Request $request)
    {
        // obtain token actor
        $data = [
            'client_id' => "actors",
            'client_secret' => '20nomalis21'
        ];
        $response = $this->successResponse($this->actorService->obtainClientId($data, Response::HTTP_CREATED));
        $responseJsonDecode = json_decode($response->getContent(), true);

        $data = [
            'grant_type' => 'client_credentials',
            'client_id' => $responseJsonDecode['data'],
            'client_secret' => base64_encode($data['client_secret']),
        ];


        $response = $this->successResponse($this->actorService->obtainActorToken($data));
        $responseJsonDecode = json_decode($response->getContent(), true);
        $token = $responseJsonDecode['access_token'];
        // dd($token);

        //Request to Actors microservice to get client_id with the name given

        //send a request to Actors microservice for cheking actors name

        $response = $this->successResponse($this->actorService->searchInternalActor(["actors" => $request->actors], ["Authorization" => $token]));
        // return $response;
        //Decode response 
        $responseJsonDecode = json_decode($response->getContent(), true);
        // return $responseJsonDecode;
        // dd($responseJsonDecode);
        //Return response Actors Not found with names given
        if (isset($responseJsonDecode['code'])) {
            return $response;
        }
        // Create data to send Request to Movie Microservice to create movie
        $data = [
            "name" => $request->name,
            "year" => $request->year,
            "actors" => $responseJsonDecode['data'],
        ];
        // return response(['data' => $data, ["access_token" => $token]], Response::HTTP_CREATED)->header('content-type', 'application/json');

        return $this->successResponse($this->movieService->createMovie($data,));
    }

    public function update(Request $request, $id)
    {
        if (isset($request->actors)) {

            // obtain token actor
            $data = [
                'client_id' => "actors",
                'client_secret' => '20nomalis21'
            ];
            $response = $this->successResponse($this->actorService->obtainClientId($data, Response::HTTP_CREATED));
            $responseJsonDecode = json_decode($response->getContent(), true);

            $data = [
                'grant_type' => 'client_credentials',
                'client_id' => $responseJsonDecode['data'],
                'client_secret' => base64_encode($data['client_secret']),
            ];


            $response = $this->successResponse($this->actorService->obtainActorToken($data));
            $responseJsonDecode = json_decode($response->getContent(), true);
            $token = $responseJsonDecode['access_token'];
            // dd($token);

            //Request to Actors microservice to get client_id with the name given

            //send a request to Actors microservice for cheking actors name

            $response = $this->successResponse($this->actorService->searchInternalActor(["actors" => $request->actors], ["Authorization" => $token]));
            // return $response;
            //Decode response 
            $responseJsonDecode = json_decode($response->getContent(), true);
            // return $responseJsonDecode;
            // dd($responseJsonDecode);
            //Return response Actors Not found with names given
            if (isset($responseJsonDecode['code'])) {
                return $response;
            }
        }

        return $this->successResponse($this->movieService->updateMovie($request->all(), $id));
    }
    public function destroy($id)
    {
        return $this->successResponse($this->movieService->deleteMovie($id));
    }

    //
    public function unserialize(Request $request)
    {

        $data = $request->data;
        return unserialize($data);
    }
}
