<?php

namespace App\Http\Controllers;

use App\services\ActorService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ActorController extends Controller
{
    use ApiResponser;

    /**
     * The service to consume the actors microservice
     *
     * @var ActorService
     */
    public $actorService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ActorService $actorService)
    {
        $this->actorService = $actorService;
    }


    /**
     * Method actorToken to obtain Access token for Actors microservices
     * @return void
     */
    public function actorToken(Request $request)
    {
        //Request to Actors microservice to get client_id with the name given
        $response = $this->successResponse($this->actorService->obtainClientId($request->all(), Response::HTTP_CREATED));
        $responseJsonDecode = json_decode($response->getContent(), true);

        $data = [
            'grant_type' => 'client_credentials',
            'client_id' => $responseJsonDecode['data'],
            'client_secret' => base64_encode($request->client_secret),
        ];
        //request to get access Token
        return  $this->successResponse($this->actorService->obtainActorToken($data));
    }
    public function index()
    {

        return $this->successResponse($this->actorService->obtainActors());
    }

    public function show($id)
    {
        return $this->successResponse($this->actorService->obtainActor($id));
    }
    public function findActor(Request $request)
    {
        return $this->successResponse($this->actorService->searchActor($request->all()));
    }

    public function store(Request $request)
    {
        return $this->successResponse($this->actorService->createActor($request->all(), Response::HTTP_CREATED));
    }

    public function update(Request $request, $id)
    {

        return $this->successResponse($this->actorService->updateActor($request->all(), $id));
    }
    public function destroy($id)
    {
        return $this->successResponse($this->actorService->deleteActor($id));
    }

    //
}
