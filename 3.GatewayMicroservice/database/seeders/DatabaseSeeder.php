<?php

namespace Database\Seeders;

use App\Models\Microservice;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Actor::factory()->times(30)->create();

        DB::table('microservices')->insert([

            [
                'nom' => 'acteurs',
                'base_url' => 'localhost:8001',
                'token' => base64_encode("actors:20nomalis21"),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'nom' => 'films',
                'base_url' => 'localhost:8002',
                'token' => base64_encode("movies:20nomalis21"),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
        // $this->call('UsersTableSeeder');
    }
}
