<?php

namespace Database\Factories;

use App\Models\Microservice;
use Illuminate\Database\Eloquent\Factories\Factory;

class MicroserviceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Microservice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'nom' => $this->faker->name,
            // 'base_url' => $this->faker->unique()->safeEmail,
            // 'token'
        ];
    }
}
