-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.4.17-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour actorapi
CREATE DATABASE IF NOT EXISTS `actorapi` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `actorapi`;

-- Listage de la structure de la table actorapi. actors
CREATE TABLE IF NOT EXISTS `actors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table actorapi.actors : ~30 rows (environ)
/*!40000 ALTER TABLE `actors` DISABLE KEYS */;
INSERT INTO `actors` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'Earline Smith', 'earline-smith', '2021-01-18 13:13:37', '2021-01-18 13:13:37'),
	(2, 'Dr. Colten Hegmann', 'dr-colten-hegmann', '2021-01-18 13:13:37', '2021-01-18 13:13:37'),
	(3, 'Isaac Beahan', 'isaac-beahan', '2021-01-18 13:13:37', '2021-01-18 13:13:37'),
	(4, 'Garnett Lueilwitz', 'garnett-lueilwitz', '2021-01-18 13:13:37', '2021-01-18 13:13:37'),
	(5, 'Una Hickle', 'una-hickle', '2021-01-18 13:13:37', '2021-01-18 13:13:37'),
	(6, 'Oma Upton', 'oma-upton', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(7, 'Joany Hintz', 'joany-hintz', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(8, 'Earlene Ziemann', 'earlene-ziemann', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(9, 'Santino Cummerata', 'santino-cummerata', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(10, 'Mohamed Pouros', 'mohamed-pouros', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(11, 'Americo Pollich', 'americo-pollich', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(12, 'Taylor Osinski', 'taylor-osinski', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(13, 'Prof. Jammie Trantow', 'prof-jammie-trantow', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(14, 'Ms. Aaliyah Tillman V', 'ms-aaliyah-tillman-v', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(15, 'Amos Lemke MD', 'amos-lemke-md', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(16, 'Lyla Kessler MD', 'lyla-kessler-md', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(17, 'Dovie Renner', 'dovie-renner', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(18, 'Renee Moore II', 'renee-moore-ii', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(19, 'Christelle Schmidt', 'christelle-schmidt', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(20, 'Tatyana Baumbach', 'tatyana-baumbach', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(21, 'Mrs. Lessie Reichert', 'mrs-lessie-reichert', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(22, 'Pedro Reinger I', 'pedro-reinger-i', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(23, 'Giovanni Abbott', 'giovanni-abbott', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(24, 'Alford Mosciski', 'alford-mosciski', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(25, 'Ms. Maiya Hegmann', 'ms-maiya-hegmann', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(26, 'Dr. Robert Goodwin', 'dr-robert-goodwin', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(27, 'Dr. Gayle Jerde', 'dr-gayle-jerde', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(28, 'Charlene Borer I', 'charlene-borer-i', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(29, 'Eulalia Welch', 'eulalia-welch', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(30, 'Winnifred Kulas', 'winnifred-kulas', '2021-01-18 13:13:38', '2021-01-18 13:13:38'),
	(31, 'fahmi mohamed reda', 'fahmi-mohamed-reda', '2021-01-18 12:41:20', '2021-01-18 12:43:19'),
	(33, 'reda amine', 'reda-amine', '2021-01-18 17:30:40', '2021-01-18 17:30:40'),
	(34, 'new actor', 'new-actor', '2021-01-20 13:13:36', '2021-01-20 13:13:36');
/*!40000 ALTER TABLE `actors` ENABLE KEYS */;

-- Listage de la structure de la table actorapi. migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table actorapi.migrations : ~6 rows (environ)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2021_01_16_162450_create_actors_table', 1),
	(2, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
	(3, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
	(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
	(5, '2016_06_01_000004_create_oauth_clients_table', 2),
	(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Listage de la structure de la table actorapi. oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table actorapi.oauth_access_tokens : ~4 rows (environ)
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
	('079e95db6095567d5f2b7c992855955170f6e30dedb857a63fa8647d8a460dfc098ac7df9e12e098', NULL, 2, NULL, '[]', 1, '2021-01-20 13:13:43', '2021-01-20 13:13:43', '2022-01-20 12:13:43'),
	('10dc05da98334770804e9bc0d6034f527d76e5df3b8f3a9622193ae3dceee4b1dfaae3104003d43c', NULL, 2, NULL, '[]', 1, '2021-01-20 12:46:18', '2021-01-20 12:46:18', '2022-01-20 11:46:18'),
	('368a6ae2116515c0133c0dd66626189bd5d63863060063f123e2987b52df08bc2ea1dc5df4424ae5', NULL, 2, NULL, '[]', 1, '2021-01-20 12:50:54', '2021-01-20 12:50:54', '2022-01-20 11:50:54'),
	('437260d8a27eb46faeab7162105b9391d9008561fd121b8a6f841cdbb2a38efe5d7334ede21958a5', NULL, 2, NULL, '[]', 1, '2021-01-20 13:00:52', '2021-01-20 13:00:52', '2022-01-20 12:00:52'),
	('5ade1877eb397c56ee06bb4134faa6471b413dad3eefd3bf2c3012b546dce9d80d6d4ca8dcfb10ea', NULL, 2, NULL, '[]', 0, '2021-01-20 19:32:21', '2021-01-20 19:32:21', '2022-01-20 18:32:21'),
	('5b4f17f3ab7d5f4af42b8bddce8f45d88e6173cb277e4e314a3b6c8409a60e1e3ab0f2e5cba82774', NULL, 2, NULL, '[]', 1, '2021-01-20 12:44:55', '2021-01-20 12:44:55', '2022-01-20 11:44:55'),
	('66f2f9b08da957feeecb3ffd6dadc935e768bd7af9ec972ebda2cc0a5edeee6ad5473113748a6109', NULL, 2, NULL, '[]', 1, '2021-01-20 19:25:24', '2021-01-20 19:25:24', '2022-01-20 18:25:24'),
	('6d27651d46927f2d19593757765fd737b0a527bd3c01a6ddc045aaf444825a6000b2bcbda74b3b10', NULL, 2, NULL, '[]', 1, '2021-01-20 12:35:02', '2021-01-20 12:35:02', '2022-01-20 11:35:02'),
	('729ec3fc740c2c0748cb648c5d303ed987d6f950dbd79ea7062d452de19775556bb7451681b640c9', NULL, 2, NULL, '[]', 1, '2021-01-20 13:01:17', '2021-01-20 13:01:17', '2022-01-20 12:01:17'),
	('7783d8bf5dd713d9f1bad783360a86c7a98ebef4c7cde0623365ce639f13585f392c9e4c9c845625', NULL, 2, NULL, '[]', 1, '2021-01-20 12:35:24', '2021-01-20 12:35:24', '2022-01-20 11:35:24'),
	('81018041e884f5421b76bd24b945e16a3ddcde58a8c052be5b1fa46e70e362439287ddbc03730d6d', NULL, 2, NULL, '[]', 1, '2021-01-20 12:34:16', '2021-01-20 12:34:16', '2022-01-20 11:34:16'),
	('94501262d28a3de2194ab844dbc1a6addfaace5e8705d3ab13c55b772423180ce08173d8514be530', NULL, 2, NULL, '[]', 1, '2021-01-20 12:43:18', '2021-01-20 12:43:18', '2022-01-20 11:43:18'),
	('acd692191d7f2826f191ff96de2c93f7692dc2151422b35eedbfb9b5fdf11fd70ee0b88ad9f9615a', NULL, 2, NULL, '[]', 1, '2021-01-20 12:45:23', '2021-01-20 12:45:23', '2022-01-20 11:45:23'),
	('b92e6b6bbc2cf1f9ffd4659854bbe234dfd3c0982d3b933d72637f4768229ea829ad1e9fbc465fb2', NULL, 2, NULL, '[]', 1, '2021-01-20 13:00:54', '2021-01-20 13:00:54', '2022-01-20 12:00:54'),
	('bc6f97790e43c24318c5880c460be0b976019b31c0c706e64f3ca3220ba8dea74f8cb3947e5fe53a', NULL, 2, NULL, '[]', 1, '2021-01-20 12:45:12', '2021-01-20 12:45:12', '2022-01-20 11:45:12'),
	('c55dd6c3fd9684d46bd77c5e04caaccf799c84251a2b5cfad52482cb1e3b174d1ff9ae0d998a39e4', NULL, 2, NULL, '[]', 1, '2021-01-20 12:43:16', '2021-01-20 12:43:16', '2022-01-20 11:43:16'),
	('df7ccac63964798e9a9fcfa26d253cc5c05ac3590f4af94459a7896211f13f1e1da9bc050a9cd11d', NULL, 2, NULL, '[]', 1, '2021-01-20 13:00:49', '2021-01-20 13:00:49', '2022-01-20 12:00:49'),
	('fcf7427a56579dd5dcdf25c14d8114a619c01438e47f65220f60e8adac1c2a6d4e6b397787ea8e8e', NULL, 2, NULL, '[]', 1, '2021-01-20 12:34:02', '2021-01-20 12:34:02', '2022-01-20 11:34:02');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Listage de la structure de la table actorapi. oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table actorapi.oauth_auth_codes : ~0 rows (environ)
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Listage de la structure de la table actorapi. oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table actorapi.oauth_clients : ~2 rows (environ)
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Lumen Personal Access Client', 'vY3qf7bQKiwbd7P3fNqPV5h77Xz5ZmoIjUpg3WAV', NULL, 'http://localhost', 1, 0, 0, '2021-01-20 12:56:38', '2021-01-20 12:56:38'),
	(2, NULL, 'actors', 'MjBub21hbGlzMjE=', NULL, 'http://localhost/auth/callback', 0, 0, 0, '2021-01-20 12:31:09', '2021-01-20 12:31:09');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Listage de la structure de la table actorapi. oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table actorapi.oauth_personal_access_clients : ~0 rows (environ)
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '2021-01-20 12:56:38', '2021-01-20 12:56:38');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Listage de la structure de la table actorapi. oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table actorapi.oauth_refresh_tokens : ~0 rows (environ)
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;


-- Listage de la structure de la base pour apigateway
CREATE DATABASE IF NOT EXISTS `apigateway` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `apigateway`;

-- Listage de la structure de la table apigateway. microservices
CREATE TABLE IF NOT EXISTS `microservices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `base_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table apigateway.microservices : ~2 rows (environ)
/*!40000 ALTER TABLE `microservices` DISABLE KEYS */;
INSERT INTO `microservices` (`id`, `nom`, `base_url`, `token`, `created_at`, `updated_at`) VALUES
	(1, 'acteurs', 'localhost:8001', 'YWN0b3JzOjIwbm9tYWxpczIx', '2021-01-17 13:57:02', '2021-01-17 13:57:02'),
	(2, 'films', 'localhost:8002', 'bW92aWVzOjIwbm9tYWxpczIx', '2021-01-17 13:57:02', '2021-01-17 13:57:02');
/*!40000 ALTER TABLE `microservices` ENABLE KEYS */;

-- Listage de la structure de la table apigateway. migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table apigateway.migrations : ~0 rows (environ)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2021_01_17_122401_create_microservices_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;


-- Listage de la structure de la base pour movieapi
CREATE DATABASE IF NOT EXISTS `movieapi` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `movieapi`;

-- Listage de la structure de la table movieapi. migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table movieapi.migrations : ~6 rows (environ)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2021_01_18_113122_create_movies_table', 1),
	(2, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
	(3, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
	(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
	(5, '2016_06_01_000004_create_oauth_clients_table', 2),
	(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Listage de la structure de la table movieapi. movies
CREATE TABLE IF NOT EXISTS `movies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `actors` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table movieapi.movies : ~48 rows (environ)
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
INSERT INTO `movies` (`id`, `name`, `slug`, `year`, `actors`, `created_at`, `updated_at`) VALUES
	(1, 'Isom VonRueden', 'isom-vonrueden', 1998, 'a:3:{i:0;a:2:{s:2:"id";i:20;s:4:"name";s:16:"Tatyana Baumbach";}i:1;a:2:{s:2:"id";i:13;s:4:"name";s:20:"Prof. Jammie Trantow";}i:2;a:2:{s:2:"id";i:20;s:4:"name";s:16:"Tatyana Baumbach";}}', '2021-01-19 14:58:00', '2021-01-19 14:58:00'),
	(2, 'Ms. Jermaine Schamberger', 'ms-jermaine-schamberger', 1984, 'a:3:{i:0;a:2:{s:2:"id";i:27;s:4:"name";s:15:"Dr. Gayle Jerde";}i:1;a:2:{s:2:"id";i:21;s:4:"name";s:20:"Mrs. Lessie Reichert";}i:2;a:2:{s:2:"id";i:6;s:4:"name";s:9:"Oma Upton";}}', '2021-01-19 14:58:00', '2021-01-19 14:58:00'),
	(3, 'Dr. Cedrick Breitenberg', 'dr-cedrick-breitenberg', 2011, 'a:3:{i:0;a:2:{s:2:"id";i:12;s:4:"name";s:14:"Taylor Osinski";}i:1;a:2:{s:2:"id";i:30;s:4:"name";s:15:"Winnifred Kulas";}i:2;a:2:{s:2:"id";i:19;s:4:"name";s:18:"Christelle Schmidt";}}', '2021-01-19 14:58:00', '2021-01-19 14:58:00'),
	(4, 'Claudine Stroman', 'claudine-stroman', 2014, 'a:3:{i:0;a:2:{s:2:"id";i:20;s:4:"name";s:16:"Tatyana Baumbach";}i:1;a:2:{s:2:"id";i:4;s:4:"name";s:17:"Garnett Lueilwitz";}i:2;a:2:{s:2:"id";i:25;s:4:"name";s:17:"Ms. Maiya Hegmann";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(5, 'Efren Borer PhD', 'efren-borer-phd', 2011, 'a:3:{i:0;a:2:{s:2:"id";i:6;s:4:"name";s:9:"Oma Upton";}i:1;a:2:{s:2:"id";i:11;s:4:"name";s:15:"Americo Pollich";}i:2;a:2:{s:2:"id";i:21;s:4:"name";s:20:"Mrs. Lessie Reichert";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(6, 'Dr. Wilfredo Harvey', 'dr-wilfredo-harvey', 1974, 'a:3:{i:0;a:2:{s:2:"id";i:27;s:4:"name";s:15:"Dr. Gayle Jerde";}i:1;a:2:{s:2:"id";i:3;s:4:"name";s:12:"Isaac Beahan";}i:2;a:2:{s:2:"id";i:24;s:4:"name";s:15:"Alford Mosciski";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(7, 'Hyman Lowe', 'hyman-lowe', 1970, 'a:3:{i:0;a:2:{s:2:"id";i:22;s:4:"name";s:15:"Pedro Reinger I";}i:1;a:2:{s:2:"id";i:18;s:4:"name";s:14:"Renee Moore II";}i:2;a:2:{s:2:"id";i:9;s:4:"name";s:17:"Santino Cummerata";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(8, 'Wilma Ankunding Jr.', 'wilma-ankunding-jr', 1978, 'a:3:{i:0;a:2:{s:2:"id";i:29;s:4:"name";s:13:"Eulalia Welch";}i:1;a:2:{s:2:"id";i:23;s:4:"name";s:15:"Giovanni Abbott";}i:2;a:2:{s:2:"id";i:3;s:4:"name";s:12:"Isaac Beahan";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(9, 'Dr. Augusta Langworth', 'dr-augusta-langworth', 1999, 'a:3:{i:0;a:2:{s:2:"id";i:2;s:4:"name";s:18:"Dr. Colten Hegmann";}i:1;a:2:{s:2:"id";i:29;s:4:"name";s:13:"Eulalia Welch";}i:2;a:2:{s:2:"id";i:25;s:4:"name";s:17:"Ms. Maiya Hegmann";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(10, 'Eunice Mueller', 'eunice-mueller', 1992, 'a:3:{i:0;a:2:{s:2:"id";i:27;s:4:"name";s:15:"Dr. Gayle Jerde";}i:1;a:2:{s:2:"id";i:20;s:4:"name";s:16:"Tatyana Baumbach";}i:2;a:2:{s:2:"id";i:31;s:4:"name";s:18:"fahmi mohamed reda";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(11, 'Harmony Bartoletti', 'harmony-bartoletti', 2020, 'a:3:{i:0;a:2:{s:2:"id";i:15;s:4:"name";s:13:"Amos Lemke MD";}i:1;a:2:{s:2:"id";i:11;s:4:"name";s:15:"Americo Pollich";}i:2;a:2:{s:2:"id";i:15;s:4:"name";s:13:"Amos Lemke MD";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(12, 'Cara Lesch MD', 'cara-lesch-md', 1980, 'a:3:{i:0;a:2:{s:2:"id";i:22;s:4:"name";s:15:"Pedro Reinger I";}i:1;a:2:{s:2:"id";i:21;s:4:"name";s:20:"Mrs. Lessie Reichert";}i:2;a:2:{s:2:"id";i:13;s:4:"name";s:20:"Prof. Jammie Trantow";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(13, 'Viviane Haag', 'viviane-haag', 1991, 'a:3:{i:0;a:2:{s:2:"id";i:8;s:4:"name";s:15:"Earlene Ziemann";}i:1;a:2:{s:2:"id";i:11;s:4:"name";s:15:"Americo Pollich";}i:2;a:2:{s:2:"id";i:6;s:4:"name";s:9:"Oma Upton";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(14, 'Gabe Turcotte V', 'gabe-turcotte-v', 2015, 'a:3:{i:0;a:2:{s:2:"id";i:29;s:4:"name";s:13:"Eulalia Welch";}i:1;a:2:{s:2:"id";i:30;s:4:"name";s:15:"Winnifred Kulas";}i:2;a:2:{s:2:"id";i:12;s:4:"name";s:14:"Taylor Osinski";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(15, 'Dixie Nader IV', 'dixie-nader-iv', 2013, 'a:3:{i:0;a:2:{s:2:"id";i:8;s:4:"name";s:15:"Earlene Ziemann";}i:1;a:2:{s:2:"id";i:19;s:4:"name";s:18:"Christelle Schmidt";}i:2;a:2:{s:2:"id";i:26;s:4:"name";s:18:"Dr. Robert Goodwin";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(16, 'Prof. Zion Torphy', 'prof-zion-torphy', 2002, 'a:3:{i:0;a:2:{s:2:"id";i:3;s:4:"name";s:12:"Isaac Beahan";}i:1;a:2:{s:2:"id";i:30;s:4:"name";s:15:"Winnifred Kulas";}i:2;a:2:{s:2:"id";i:19;s:4:"name";s:18:"Christelle Schmidt";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(17, 'Neha Lang', 'neha-lang', 1970, 'a:3:{i:0;a:2:{s:2:"id";i:18;s:4:"name";s:14:"Renee Moore II";}i:1;a:2:{s:2:"id";i:18;s:4:"name";s:14:"Renee Moore II";}i:2;a:2:{s:2:"id";i:21;s:4:"name";s:20:"Mrs. Lessie Reichert";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(18, 'Alexandra Rosenbaum', 'alexandra-rosenbaum', 1999, 'a:3:{i:0;a:2:{s:2:"id";i:17;s:4:"name";s:12:"Dovie Renner";}i:1;a:2:{s:2:"id";i:9;s:4:"name";s:17:"Santino Cummerata";}i:2;a:2:{s:2:"id";i:28;s:4:"name";s:16:"Charlene Borer I";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(19, 'Dr. Kaylie Huel DDS', 'dr-kaylie-huel-dds', 2000, 'a:3:{i:0;a:2:{s:2:"id";i:6;s:4:"name";s:9:"Oma Upton";}i:1;a:2:{s:2:"id";i:15;s:4:"name";s:13:"Amos Lemke MD";}i:2;a:2:{s:2:"id";i:3;s:4:"name";s:12:"Isaac Beahan";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(20, 'Timmy Hudson', 'timmy-hudson', 1985, 'a:3:{i:0;a:2:{s:2:"id";i:19;s:4:"name";s:18:"Christelle Schmidt";}i:1;a:2:{s:2:"id";i:26;s:4:"name";s:18:"Dr. Robert Goodwin";}i:2;a:2:{s:2:"id";i:15;s:4:"name";s:13:"Amos Lemke MD";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(21, 'Marcellus Thiel', 'marcellus-thiel', 1991, 'a:3:{i:0;a:2:{s:2:"id";i:21;s:4:"name";s:20:"Mrs. Lessie Reichert";}i:1;a:2:{s:2:"id";i:10;s:4:"name";s:14:"Mohamed Pouros";}i:2;a:2:{s:2:"id";i:30;s:4:"name";s:15:"Winnifred Kulas";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(22, 'Ms. Katarina Murray II', 'ms-katarina-murray-ii', 2009, 'a:3:{i:0;a:2:{s:2:"id";i:30;s:4:"name";s:15:"Winnifred Kulas";}i:1;a:2:{s:2:"id";i:27;s:4:"name";s:15:"Dr. Gayle Jerde";}i:2;a:2:{s:2:"id";i:9;s:4:"name";s:17:"Santino Cummerata";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(23, 'Easton Zemlak', 'easton-zemlak', 1980, 'a:3:{i:0;a:2:{s:2:"id";i:26;s:4:"name";s:18:"Dr. Robert Goodwin";}i:1;a:2:{s:2:"id";i:31;s:4:"name";s:18:"fahmi mohamed reda";}i:2;a:2:{s:2:"id";i:22;s:4:"name";s:15:"Pedro Reinger I";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(24, 'Mr. Orville Wolff IV', 'mr-orville-wolff-iv', 1983, 'a:3:{i:0;a:2:{s:2:"id";i:19;s:4:"name";s:18:"Christelle Schmidt";}i:1;a:2:{s:2:"id";i:18;s:4:"name";s:14:"Renee Moore II";}i:2;a:2:{s:2:"id";i:23;s:4:"name";s:15:"Giovanni Abbott";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(25, 'Herminio Pfeffer', 'herminio-pfeffer', 1982, 'a:3:{i:0;a:2:{s:2:"id";i:29;s:4:"name";s:13:"Eulalia Welch";}i:1;a:2:{s:2:"id";i:15;s:4:"name";s:13:"Amos Lemke MD";}i:2;a:2:{s:2:"id";i:27;s:4:"name";s:15:"Dr. Gayle Jerde";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(26, 'Kadin Christiansen', 'kadin-christiansen', 1992, 'a:3:{i:0;a:2:{s:2:"id";i:28;s:4:"name";s:16:"Charlene Borer I";}i:1;a:2:{s:2:"id";i:8;s:4:"name";s:15:"Earlene Ziemann";}i:2;a:2:{s:2:"id";i:12;s:4:"name";s:14:"Taylor Osinski";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(27, 'Pietro O\'Hara', 'pietro-ohara', 1995, 'a:3:{i:0;a:2:{s:2:"id";i:31;s:4:"name";s:18:"fahmi mohamed reda";}i:1;a:2:{s:2:"id";i:6;s:4:"name";s:9:"Oma Upton";}i:2;a:2:{s:2:"id";i:25;s:4:"name";s:17:"Ms. Maiya Hegmann";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(28, 'Cathrine Veum', 'cathrine-veum', 1974, 'a:3:{i:0;a:2:{s:2:"id";i:15;s:4:"name";s:13:"Amos Lemke MD";}i:1;a:2:{s:2:"id";i:2;s:4:"name";s:18:"Dr. Colten Hegmann";}i:2;a:2:{s:2:"id";i:16;s:4:"name";s:15:"Lyla Kessler MD";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(29, 'Allene Hahn', 'allene-hahn', 1999, 'a:3:{i:0;a:2:{s:2:"id";i:8;s:4:"name";s:15:"Earlene Ziemann";}i:1;a:2:{s:2:"id";i:4;s:4:"name";s:17:"Garnett Lueilwitz";}i:2;a:2:{s:2:"id";i:23;s:4:"name";s:15:"Giovanni Abbott";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(30, 'Werner Hyatt', 'werner-hyatt', 2012, 'a:3:{i:0;a:2:{s:2:"id";i:17;s:4:"name";s:12:"Dovie Renner";}i:1;a:2:{s:2:"id";i:20;s:4:"name";s:16:"Tatyana Baumbach";}i:2;a:2:{s:2:"id";i:1;s:4:"name";s:13:"Earline Smith";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(31, 'Gardner Swift', 'gardner-swift', 2011, 'a:3:{i:0;a:2:{s:2:"id";i:9;s:4:"name";s:17:"Santino Cummerata";}i:1;a:2:{s:2:"id";i:15;s:4:"name";s:13:"Amos Lemke MD";}i:2;a:2:{s:2:"id";i:10;s:4:"name";s:14:"Mohamed Pouros";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(32, 'Stefanie Runte', 'stefanie-runte', 2017, 'a:3:{i:0;a:2:{s:2:"id";i:11;s:4:"name";s:15:"Americo Pollich";}i:1;a:2:{s:2:"id";i:28;s:4:"name";s:16:"Charlene Borer I";}i:2;a:2:{s:2:"id";i:27;s:4:"name";s:15:"Dr. Gayle Jerde";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(33, 'Lester Hartmann', 'lester-hartmann', 2007, 'a:3:{i:0;a:2:{s:2:"id";i:17;s:4:"name";s:12:"Dovie Renner";}i:1;a:2:{s:2:"id";i:16;s:4:"name";s:15:"Lyla Kessler MD";}i:2;a:2:{s:2:"id";i:23;s:4:"name";s:15:"Giovanni Abbott";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(35, 'Edwardo Cole', 'edwardo-cole', 2004, 'a:3:{i:0;a:2:{s:2:"id";i:21;s:4:"name";s:20:"Mrs. Lessie Reichert";}i:1;a:2:{s:2:"id";i:18;s:4:"name";s:14:"Renee Moore II";}i:2;a:2:{s:2:"id";i:25;s:4:"name";s:17:"Ms. Maiya Hegmann";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(36, 'Dr. Johnnie Graham', 'dr-johnnie-graham', 1975, 'a:3:{i:0;a:2:{s:2:"id";i:9;s:4:"name";s:17:"Santino Cummerata";}i:1;a:2:{s:2:"id";i:18;s:4:"name";s:14:"Renee Moore II";}i:2;a:2:{s:2:"id";i:13;s:4:"name";s:20:"Prof. Jammie Trantow";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(37, 'Ms. Eulah Rolfson MD', 'ms-eulah-rolfson-md', 2002, 'a:3:{i:0;a:2:{s:2:"id";i:24;s:4:"name";s:15:"Alford Mosciski";}i:1;a:2:{s:2:"id";i:25;s:4:"name";s:17:"Ms. Maiya Hegmann";}i:2;a:2:{s:2:"id";i:13;s:4:"name";s:20:"Prof. Jammie Trantow";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(38, 'Prof. Tara Schiller', 'prof-tara-schiller', 2001, 'a:3:{i:0;a:2:{s:2:"id";i:9;s:4:"name";s:17:"Santino Cummerata";}i:1;a:2:{s:2:"id";i:11;s:4:"name";s:15:"Americo Pollich";}i:2;a:2:{s:2:"id";i:23;s:4:"name";s:15:"Giovanni Abbott";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(39, 'Miller Crona', 'miller-crona', 2015, 'a:3:{i:0;a:2:{s:2:"id";i:16;s:4:"name";s:15:"Lyla Kessler MD";}i:1;a:2:{s:2:"id";i:15;s:4:"name";s:13:"Amos Lemke MD";}i:2;a:2:{s:2:"id";i:26;s:4:"name";s:18:"Dr. Robert Goodwin";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(40, 'Gustave Weimann', 'gustave-weimann', 1976, 'a:3:{i:0;a:2:{s:2:"id";i:17;s:4:"name";s:12:"Dovie Renner";}i:1;a:2:{s:2:"id";i:26;s:4:"name";s:18:"Dr. Robert Goodwin";}i:2;a:2:{s:2:"id";i:24;s:4:"name";s:15:"Alford Mosciski";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(41, 'Dangelo Larkin', 'dangelo-larkin', 1985, 'a:3:{i:0;a:2:{s:2:"id";i:18;s:4:"name";s:14:"Renee Moore II";}i:1;a:2:{s:2:"id";i:29;s:4:"name";s:13:"Eulalia Welch";}i:2;a:2:{s:2:"id";i:9;s:4:"name";s:17:"Santino Cummerata";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(42, 'Baron Roberts', 'baron-roberts', 2002, 'a:3:{i:0;a:2:{s:2:"id";i:31;s:4:"name";s:18:"fahmi mohamed reda";}i:1;a:2:{s:2:"id";i:4;s:4:"name";s:17:"Garnett Lueilwitz";}i:2;a:2:{s:2:"id";i:8;s:4:"name";s:15:"Earlene Ziemann";}}', '2021-01-19 14:58:01', '2021-01-19 14:58:01'),
	(43, 'Prof. Trevor Cummerata I', 'prof-trevor-cummerata-i', 2015, 'a:3:{i:0;a:2:{s:2:"id";i:19;s:4:"name";s:18:"Christelle Schmidt";}i:1;a:2:{s:2:"id";i:15;s:4:"name";s:13:"Amos Lemke MD";}i:2;a:2:{s:2:"id";i:8;s:4:"name";s:15:"Earlene Ziemann";}}', '2021-01-19 14:58:02', '2021-01-19 14:58:02'),
	(44, 'Alycia Turner', 'alycia-turner', 1977, 'a:3:{i:0;a:2:{s:2:"id";i:23;s:4:"name";s:15:"Giovanni Abbott";}i:1;a:2:{s:2:"id";i:30;s:4:"name";s:15:"Winnifred Kulas";}i:2;a:2:{s:2:"id";i:20;s:4:"name";s:16:"Tatyana Baumbach";}}', '2021-01-19 14:58:02', '2021-01-19 14:58:02'),
	(45, 'Destiney Kutch', 'destiney-kutch', 1981, 'a:3:{i:0;a:2:{s:2:"id";i:4;s:4:"name";s:17:"Garnett Lueilwitz";}i:1;a:2:{s:2:"id";i:1;s:4:"name";s:13:"Earline Smith";}i:2;a:2:{s:2:"id";i:27;s:4:"name";s:15:"Dr. Gayle Jerde";}}', '2021-01-19 14:58:02', '2021-01-19 14:58:02'),
	(46, 'Noel Feeney Jr.', 'noel-feeney-jr', 2006, 'a:3:{i:0;a:2:{s:2:"id";i:24;s:4:"name";s:15:"Alford Mosciski";}i:1;a:2:{s:2:"id";i:27;s:4:"name";s:15:"Dr. Gayle Jerde";}i:2;a:2:{s:2:"id";i:16;s:4:"name";s:15:"Lyla Kessler MD";}}', '2021-01-19 14:58:02', '2021-01-19 14:58:02'),
	(47, 'Luna Walker', 'luna-walker', 1974, 'a:3:{i:0;a:2:{s:2:"id";i:19;s:4:"name";s:18:"Christelle Schmidt";}i:1;a:2:{s:2:"id";i:29;s:4:"name";s:13:"Eulalia Welch";}i:2;a:2:{s:2:"id";i:12;s:4:"name";s:14:"Taylor Osinski";}}', '2021-01-19 14:58:02', '2021-01-19 14:58:02'),
	(48, 'Bradley Denesik', 'bradley-denesik', 2018, 'a:3:{i:0;a:2:{s:2:"id";i:14;s:4:"name";s:21:"Ms. Aaliyah Tillman V";}i:1;a:2:{s:2:"id";i:19;s:4:"name";s:18:"Christelle Schmidt";}i:2;a:2:{s:2:"id";i:18;s:4:"name";s:14:"Renee Moore II";}}', '2021-01-19 14:58:02', '2021-01-19 14:58:02'),
	(49, 'Camille Rice', 'camille-rice', 1977, 'a:3:{i:0;a:2:{s:2:"id";i:29;s:4:"name";s:13:"Eulalia Welch";}i:1;a:2:{s:2:"id";i:19;s:4:"name";s:18:"Christelle Schmidt";}i:2;a:2:{s:2:"id";i:24;s:4:"name";s:15:"Alford Mosciski";}}', '2021-01-19 14:58:02', '2021-01-19 14:58:02');
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;

-- Listage de la structure de la table movieapi. oauth_access_tokens
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table movieapi.oauth_access_tokens : ~0 rows (environ)
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
	('162d007dd51e2c6d5e1935aee35da6cb271cd24395dea28df5008cc39fa64b87c107c347c1d33f7b', NULL, 2, NULL, '[]', 0, '2021-01-20 19:39:35', '2021-01-20 19:39:35', '2022-01-20 18:39:35'),
	('857a6bf546f19a046e031bb77fa921cb136ec6d69af183c102832a649b3c23ee39c80b367ac5d991', NULL, 2, NULL, '[]', 1, '2021-01-20 19:23:06', '2021-01-20 19:23:06', '2022-01-20 18:23:06'),
	('9dcacd9d636e758304c1e06e366ad883358f02744930a24e443c1ea96df6962904a437fc6a441ed5', NULL, 2, NULL, '[]', 1, '2021-01-20 19:34:24', '2021-01-20 19:34:24', '2022-01-20 18:34:24');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;

-- Listage de la structure de la table movieapi. oauth_auth_codes
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table movieapi.oauth_auth_codes : ~0 rows (environ)
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;

-- Listage de la structure de la table movieapi. oauth_clients
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table movieapi.oauth_clients : ~0 rows (environ)
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
	(1, NULL, 'Lumen Personal Access Client', 'b6n3dspjRaKsRVE9UQzCtu7MxeqtyUqg6NFgtY7T', NULL, 'http://localhost', 1, 0, 0, '2021-01-20 19:19:13', '2021-01-20 19:19:13'),
	(2, NULL, 'movies', 'MjBub21hbGlzMjE=', NULL, 'http://localhost/auth/callback', 0, 0, 0, '2021-01-20 19:22:17', '2021-01-20 19:22:17');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;

-- Listage de la structure de la table movieapi. oauth_personal_access_clients
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table movieapi.oauth_personal_access_clients : ~0 rows (environ)
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
	(1, 1, '2021-01-20 19:19:13', '2021-01-20 19:19:13');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;

-- Listage de la structure de la table movieapi. oauth_refresh_tokens
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table movieapi.oauth_refresh_tokens : ~0 rows (environ)
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
