<?php

namespace Database\Factories;

use App\Models\Movie;
use GuzzleHttp\client;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


class MovieFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Movie::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        //Request actors from Actor microservice
        $client = new client([
            'base_uri' => 'localhost:8001',
        ]);
        // if (isset($this->secret)) {

        //     $headers['Authorization'] = $this->secret;
        // }

        $response = $client->request('GET', '/actors/all', ['form_params' => [], 'headers' => []]);

        //format data from response Actor microservice
        $datasFormat = [];
        $datas = json_decode($response->getBody(), true)['data'];      
        foreach ($datas as $key => $data) {
            array_push($datasFormat, ["id" => $data['id'], "name" => $data['name']]);
        }
        //Get 3 random Actor and store it in array
        $randActor = [];
        for ($i = 0; $i < 3; $i++) {
            array_push($randActor, $datasFormat[array_rand($datasFormat)]);
        }
        return [
            'name' => $name = $this->faker->name,
            'slug' => Str::slug($name, '-'),
            'year' => $this->faker->year($max = 'now'),
            'actors' => serialize($randActor)

        ];
    }
}
