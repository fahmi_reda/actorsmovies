<?php

namespace App\Http\Resources;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {


        $obj2 = json_decode(json_encode(unserialize($this->actors), JSON_FORCE_OBJECT));


        return [
            'id' => $this->id,
            'name' => $this->name,


            /**
             * show Actors as JSON
             */
            // 'actors' => $obj2,

            /**
             * show Actors as Array
             */
            'year' => $this->year,
            'actors' => unserialize($this->actors),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
