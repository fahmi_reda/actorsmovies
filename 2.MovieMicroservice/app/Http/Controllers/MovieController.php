<?php

namespace App\Http\Controllers;

use App\Http\Resources\MovieResource;
use App\Models\Movie;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use GuzzleHttp\client;
use Illuminate\Database\Eloquent\Collection;
use Laravel\Passport\Passport;


class MovieController extends Controller
{
    //trait for custom response Api call
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function movieToken(Request $request)
    {
        $client = Passport::client()->where('name', $request->client_id)->first();
        if (!isset($client)) {
            return $this->errorResponse('Client not found', Response::HTTP_NOT_FOUND);
        }
        return $this->successResponse($client->id);
    }


    /**
     * Method index
     * @return JSON
     */
    public function index(Request $request)
    {



        $movies = Movie::orderBy('created_at', 'desc')->get();
        // foreach ($movies as $key => $movie) {

        //     return $movie->id;
        // }
        return $this->successResponse(MovieResource::collection($movies));
        return $movies;

        // return $this->successResponse($movies);
    }

    /**
     * Method show
     * @return JSON
     */
    public function show($id)
    {
        $movie = Movie::findOrFail($id);
        // return $movie;
        return $this->successResponse(new MovieResource($movie));

        // return $this->successResponse($movie);
    }

    /**
     * Method Movies by year
     * @return JSON
     */
    public function  movieByYear($year)
    {
        $movies = Movie::where('year', $year)->get();
        return $this->successResponse(MovieResource::collection($movies));
    }

    /**
     * Method Movies by Actor ID

     * @return void
     */
    public function  movieByActorId($id)
    {

        // return $id;
        $movies = Movie::all();
        // return $movies;
        $filtered = $movies->filter(function ($value, $key) use ($id) {


            foreach (unserialize($value->actors) as  $item) {

                if (array_search($id, $item)) {

                    return $value;
                }
            }
        });
        // return $filtred;

        return $this->successResponse(MovieResource::collection($filtered->values()->all()));
    }



    /**
     * Method store
     * @return JSON
     */
    public function store(Request $request)
    {
        // return $request->all();
        // $client = new client([
        //     'base_uri' => 'localhost:8001',
        // ]);
        // // if (isset($this->secret)) {

        // //     $headers['Authorization'] = $this->secret;
        // // }

        // $response = $client->request('GET', '/movies/all', ['form_params' => [], 'headers' => []]);
        // return $response;
        $rules = [
            'name' => 'required|max:255',
            'year' => 'required|max:2030|integer',
            'actors' => 'required|max:255|array',

        ];
        $this->validate($request, $rules);

        $movie = Movie::create([
            'name' => $request->name,
            'slug' => Str::slug($request->name, '-'),
            'year' => $request->year,
            'actors' => serialize($request->actors),
        ]);
        return $this->successResponse(new MovieResource($movie), Response::HTTP_CREATED);
    }


    /**
     * Method update
     * @return JSON
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'max:255',
            'year' => 'max:2030|integer',
            'actors' => 'max:255',
        ];
        $this->validate($request, $rules);
        $movie = Movie::findOrFail($id);
        $movie->fill($request->all());

        if ($request->name) {

            $movie->slug = Str::slug($request->name);
        }
        if ($request->actors) {

            $movie->actors = serialize($request->actors);
        }
        //check if atleast 1 data changed in case we have more input.
        if ($movie->isClean()) {
            return $this->errorResponse('atleast one value must change', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $movie->save();
        return $this->successResponse(new MovieResource($movie));
    }

    /**
     * Method destroy
     * @return JSON
     */
    public function destroy($id)
    {
        $movie = Movie::findOrFail($id);
        $movie->delete();
        return $this->successResponse(new MovieResource($movie));
    }

    public function findMovie(Request $request)
    {
        // $movies = unserialize($request->movies);
        // return $request->movies;
        // return serialize($request->movies);

        $moviesData = [];
        $moviesNotFound = [];
        $rules = [
            'movies' => 'required|array',

        ];
        $this->validate($request, $rules);

        foreach ($request->movies as $key => $movie) {


            $data = Movie::where('slug', 'like',  Str::slug($movie, '-'))->first();

            if ($data) {

                array_push($moviesData, ['id' => $data->id, 'name' => $data->name]);
            } else {
                array_push($moviesNotFound, $movie);
            }
        }
        if ($moviesNotFound) {
            return $this->errorResponse(["movieNotFound" => $moviesNotFound], Response::HTTP_NOT_FOUND);
        } else {
            return $moviesData;
        }
    }
}
