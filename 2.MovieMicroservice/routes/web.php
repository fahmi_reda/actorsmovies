<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * Get Token Actor microservice
 */
$router->post('/movies/token', 'MovieController@movieToken');

$router->group(['middleware' => 'client.credentials'], function () use ($router) {
    
    
    $router->get('/movies/all', 'MovieController@index');
    $router->get('/movie/read/{id}', 'MovieController@show');
    $router->post('/movie/create', 'MovieController@store');
    $router->get('/movie/{year}', 'MovieController@movieByYear');
    $router->get('/movie/actor/{id}', 'MovieController@movieByActorId');
    $router->put('/movie/update/{id}', 'MovieController@update');
    $router->patch('/movie/update/{id}', 'MovieController@update');
    $router->delete('/movie/delete/{id}', 'MovieController@destroy');
});
