import React from "react";
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect
} from "react-router-dom";

// import Home from "./pages/Home";
import Ha from "./components/Ha";

// import Live from "./pages/Live";
import "./App.css";
import Home from "./components/Home";
import Actors from "./components/Actors";
import Mov from "./components/Mov";
import ActorSingleS from "./components/ActorSingles";
import MovieSingle from "./components/MovieSingle";

// import Register from "./pages/Register";
// import Login from "./pages/Login";
// import Favoris from "./pages/Favoris";

const App = props => {
  // const BrowserHistory = require("react-router/lib/BrowserHistory").default;
  // useEffect(() => {

  // }, [])
  return (
    <>
      <Router>
        {/*Navbar */}
        <Route path="/" render={props => <Ha {...props} />} />
        <div className="container mt-5 ">
          <div className="react-cont">
            <Switch>
              {/* Home */}
              <Route
                exact
                path="/"
                render={props => (
                  <Home
                    exact
                    {...props}
                  // loggedInStatus={loggedInStatus}
                  // handleLogout={handleLogout}
                  />
                )}
              />
              {/* Actors */}

              <Route
                exact
                path="/actors"
                render={props => <Actors exact {...props} />}
              />
              <Route
                exact
                path="/actors/:id"
                render={props => <ActorSingleS exact {...props} />}
              />
              <Route
                exact
                path="/movies"
                render={props => (
                  <Mov
                    exact
                    {...props}
                  // loggedInStatus={loggedInStatus}
                  // handleLogout={handleLogout}
                  />
                )}
              />
              <Route
                exact
                path="/movies/:id"
                render={props => (
                  <MovieSingle
                    exact
                    {...props}
                  // loggedInStatus={loggedInStatus}
                  // handleLogout={handleLogout}
                  />
                )}
              />
              {/* <Route
              exact
              path="/signin"
              render={props => (
                <Login
                  exact
                  {...props}
                  // loggedInStatus={loggedInStatus}
                  // handleLogout={handleLogout}
                />
              )}
            /> */}
              {/* <Route
              exact
              path="/signup"
              render={props => (
                <Register
                  exact
                  {...props}
                  // loggedInStatus={loggedInStatus}
                  // handleLogout={handleLogout}
                />
              )}
            /> */}
              {/* <Route
              exact
              path="/favoris"
              render={props => (
                <Favoris
                  exact
                  {...props}
                  // loggedInStatus={loggedInStatus}
                  // handleLogout={handleLogout}
                />
              )}
            /> */}
            </Switch>
          </div>
        </div>
      </Router>
    </>
  );
};
export default App;
