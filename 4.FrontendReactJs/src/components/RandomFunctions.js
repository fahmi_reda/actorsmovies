export const mysql_date_to_js_date = mysql_date => {
  var parts = mysql_date.split("-");
  if (parts.length != 3) {
    return undefined;
  }
  var year = parseInt(parts[0]);
  var month = parseInt(parts[1]) - 1; // months indexes are zero based, e.g. 9 == Octobre
  var day = parseInt(parts[2]);
  return new Date(year, month, day).toDateString();
};
