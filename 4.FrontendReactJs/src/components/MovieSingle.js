import React, { useState, useEffect } from "react";
import { getSingleMovie, getFilmsByMovie } from "./MoviesFunctions";
import { Card, Button, Badge, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { mysql_date_to_js_date } from "./RandomFunctions";

const MovieSingle = (props) => {
    const id = props.match.params.id;
    const [refresh, setRefresh] = useState(false);
    const [data, setData] = useState("");
    const [actors, setActors] = useState([]);
    const [filmsByMovie, setFilmsByMovie] = useState([]);
    const [errorMsg, setErrorMsg] = useState({ msg: "", status: "" });



    useEffect(() => {
        setErrorMsg({ msg: "", status: "" });
        getSingleMovie(id).then((res) => {
            setTimeout(() => {
                // setMoviesStatus(true);
            }, 1000);
            if (res.status == 401 || res.status == 500) {
                // setError({ msg: res.data.error, status: true });
                // console.log(error.msg);
            } else if (res && res.status == 200) {
                console.log(res);
                setData(res.data.data);
                setActors(res.data.data.actors)
                console.log(res.data.data.actors)
            }
        });
        // getFilmsByMovie(id).then((res) => {
        //   setTimeout(() => {
        //     // setMoviesStatus(true);
        //   }, 1000);
        //   if (res.status == 401 || res.status == 500) {
        // console.log(res)
        //     setErrorMsg({
        //       msg: "Movies microservice offline",
        //       status: "text-danger",
        //     });
        //   } else if (res.status == 200) {
        //     setFilmsByMovie(res.data.data);
        //     console.log("res 200");
        //     if (Array.isArray(res.data.data) && !res.data.data.length) {
        //       setErrorMsg({ msg: "No Movies for this Movie", status: "text-info" });
        //     }
        //     console.log(res);
        //   }
        // });
    }, [refresh]);

    return (
        <Card>
            <Card.Header className="text-capitalize ">
                <h2>                    {data.name}                </h2>
            </Card.Header>
            <Card.Body className="mb-5">
                <Card.Title>Year</Card.Title>
                <h2 className="text-primary ml-3 ">{data.year}</h2>
                <Card.Title>Actors</Card.Title>
                {actors.map((item, i) => (
                    <Link className=" c-white" to={`/actors/${item.id}`}>
                        <Button
                            className="mr-2"
                            variant="dark"
                        // onClick={showActor}
                        >
                            {item.name}
                        </Button>
                    </Link>
                    // <div>{item.name}</div>
                ))}


                {/* {errorMsg && <div className={errorMsg.status}>{errorMsg.msg}</div>}
        {filmsByMovie.map((item, i) => (
          <>
            <Row key={i}>
              <Col sm={2}>
                <Badge className="mr-3" variant="info">
                  {item.name}
                </Badge>
              </Col>
              <Col sm={1}>
                <Badge variant="secondary">{item.year}</Badge>
              </Col>
            </Row>
          </>
        ))} */}

                {/* <Card.Text className="text-muted">Date: {data.created_at}</Card.Text> */}
            </Card.Body>
            <Card.Footer className="text-capitalize ">
                <Link className=" c-white" to={`/movies`}>
                    <Button variant="primary">Back</Button>
                </Link>
            </Card.Footer>
        </Card>
    );
};

export default MovieSingle;
