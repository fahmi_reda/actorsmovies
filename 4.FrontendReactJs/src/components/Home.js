import React, { useEffect, useState } from "react";
import {
  Alert,
  Button,
  Card,
  Col,
  Container,
  Form,
  Row,
} from "react-bootstrap";
import actors from "./../img/actors.jpg";
import movies from "./../img/movies.jpg";
import { actorsToken, moviesToken } from "./TokensFunctions";

const Home = (props) => {
  // const [actorsClientId, setActorsClientId] = useState("");
  // const [actorsClientSecret, setActorsClientSecret] = useState("");
  // const [moviesClientId, setMoviesClientId] = useState("");
  // const [moviesClientSecret, setMoviesClientSecret] = useState("");
  const [clientCredentials, setClientCredentials] = useState({});
  const [responseMessage, setResponseMessage] = useState({
    msg: "",
    status: false,
  });

  useEffect(() => {
    setResponseMessage({ msg: "", status: false });
  }, []);
  // localStorage.removeItem("moviesToken");
  const actorsOnChange = (e) => {
    setClientCredentials({
      ...clientCredentials,
      [e.target.id]: e.target.value,
    });
    // console.log(clientCredentials);
  };
  //Connect microservice Actors
  const actorsSubmit = (e) => {
    localStorage.removeItem("actorsToken");
    e.preventDefault();

    console.log(clientCredentials);
    localStorage.setItem("actorsClientId", clientCredentials.actorsClientId);
    localStorage.setItem(
      "actorsClientSecret",
      clientCredentials.actorsClientSecret
    );
    const actorsCredentials = {
      client_id: localStorage.actorsClientId,
      client_secret: localStorage.actorsClientSecret,
    };
    actorsToken(actorsCredentials).then((res) => {
      if (!res) {
        setResponseMessage({
          msg: "Alert :Server optaskfline",
          status: true,
          color: "danger",
        });
      } else if (res.status == 404) {
        setResponseMessage({
          msg: `Alert : ${res.data.error}`,
          status: true,
          color: "danger",
        });
      } else if (res.status == 401) {
        setResponseMessage({
          msg: `Alert : Invalid Client secret`,
          status: true,
          color: "danger",
        });
      } else if (res.status == 200) {
        setResponseMessage({
          msg: `Alert : Actors Microservice Connected`,
          status: true,
          color: "success",
        });
      }
      console.log(res);

      props.history.push("/");
    });
    //Send token Actors Request

    console.log(localStorage);
  };


  //movies microservices
  const moviesOnChange = (e) => {
    setClientCredentials({
      ...clientCredentials,
      [e.target.id]: e.target.value,
    });
    // console.log(clientCredentials);
  };
  //Connect microservice Actors
  const moviesSubmit = (e) => {
    localStorage.removeItem("moviesToken");
    e.preventDefault();

    console.log(clientCredentials);
    localStorage.setItem("moviesClientId", clientCredentials.moviesClientId);
    localStorage.setItem(
      "moviesClientSecret",
      clientCredentials.moviesClientSecret
    );
    const moviesCredentials = {
      client_id: localStorage.moviesClientId,
      client_secret: localStorage.moviesClientSecret,
    };
    moviesToken(moviesCredentials).then((res) => {
      console.log("status of response:", res.status)
      if (!res) {

        setResponseMessage({
          msg: "Alert :Server offline",
          status: true,
          color: "danger",
        });
      } else if (res.status == 404) {
        setResponseMessage({
          msg: `Alert : ${res.data.error}`,
          status: true,
          color: "danger",
        });
      } else if (res.status == 401) {
        setResponseMessage({
          msg: `Alert : Invalid Client secret`,
          status: true,
          color: "danger",
        });
      } else if (res.status == 200) {

        setResponseMessage({
          msg: `Alert : movies Microservice Connected`,
          status: true,
          color: "success",
        });
      }
      console.log(res);

      props.history.push("/");
    });
    //Send token Actors Request

    console.log(localStorage);
  };


  return (
    <>
      <Container>
        {responseMessage.status && (
          <Alert variant={responseMessage.color}>{responseMessage.msg}</Alert>
        )}

        <Row className="justify-content-md-center">
          <Col xs lg="4">
            <Card style={{ width: "18rem" }}>
              <Card.Img variant="top" src={actors} />
              <Card.Body>
                <Card.Title>Actors Microservice</Card.Title>

                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Client_id</Form.Label>
                  {/* <Form.Control
                    type="text"
                    id="actorsClientId"
                    onChange={actorsOnChange}
                  /> */}
                  <input
                    type="text"
                    name=""
                    id="actorsClientId"
                    onChange={actorsOnChange}
                  />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Client_secret</Form.Label>
                  <input
                    type="password"
                    name=""
                    id="actorsClientSecret"
                    onChange={actorsOnChange}
                  />
                </Form.Group>
                {/* <Form.Group controlId="formBasicCheckbox">
                  <Form.Check type="checkbox" label="Check me out" />
                </Form.Group> */}

                <Button variant="primary" onClick={actorsSubmit}>
                  Connect
                </Button>
              </Card.Body>
            </Card>
          </Col>

          {/* movies microservice */}
          <Col xs lg="4">
            <Card style={{ width: "18rem" }}>
              <Card.Img variant="top" src={movies} />
              <Card.Body>
                <Card.Title>Movies Microservice</Card.Title>

                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Client_id</Form.Label>

                  <input
                    type="text"
                    name=""
                    id="moviesClientId"
                    onChange={moviesOnChange}
                  />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                  <Form.Label>Client_secret</Form.Label>
                  <input
                    type="password"
                    name=""
                    id="moviesClientSecret"
                    onChange={moviesOnChange}
                  />
                </Form.Group>
                {/* <Form.Group controlId="formBasicCheckbox">
                  <Form.Check type="checkbox" label="Check me out" />
                </Form.Group> */}

                <Button variant="primary" onClick={moviesSubmit}>
                  Connect
                </Button>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
};
export default Home;
