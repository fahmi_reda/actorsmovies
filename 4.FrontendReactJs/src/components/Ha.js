import React, { Component, useEffect, useState } from "react";
import {
  Button,
  Navbar,
  NavDropdown,
  Nav,
  Form,
  FormControl,
  Modal,
  Row,
  Toast,
  Col,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { actorsToken, moviesToken } from "./TokensFunctions";

const Test = (props) => {
  const [show, setShow] = useState(false);
  const [responseMessage, setResponseMessage] = useState({
    msg: "",
    status: false,
  });
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  useEffect(() => {
    setResponseMessage({ msg: "", status: false });
  }, []);
  // const [show, setShow] = useState(false);

  // const handleClose = () => setShow(false);
  // const handleShow = () => setShow(true);
  //   did Mount

  const disconnectActors = (e) => {
    e.preventDefault();
    localStorage.removeItem("actorsToken");
    props.history.push("/");
  };
  const disconnectMovies = (e) => {
    e.preventDefault();
    localStorage.removeItem("moviesToken");
    props.history.push("/");
  };
  const connectActors = (e) => {
    e.preventDefault();

    console.log(localStorage);
    if (!localStorage.actorsClientId || !localStorage.actorsClientSecret) {
      handleShow();
    } else {
      const actorsCredentials = {
        client_id: localStorage.actorsClientId,
        client_secret: localStorage.actorsClientSecret,
      };

      actorsToken(actorsCredentials).then((res) => {
        if (!res) {
          setResponseMessage({ msg: "server offline", status: true });
        } else if (res.status != 200) {
            handleShow();
        }
        props.history.push("/actors");
      });
    }
  };
  const changeCrendential = (e) => {
    e.preventDefault();

    props.history.push("/");
    handleClose();
  };

  const connectMovies = (e) => {
    e.preventDefault();

    console.log(localStorage);
    if (!localStorage.moviesClientId || !localStorage.moviesClientSecret) {
      handleShow();
    } else {
      const moviesCredentials = {
        client_id: localStorage.moviesClientId,
        client_secret: localStorage.moviesClientSecret,
      };

      moviesToken(moviesCredentials).then((res) => {
        if (!res) {
          setResponseMessage({ msg: "server offline", status: true });
        } else if (res.status != 200) {
            handleShow();
        }
        props.history.push("/movies");
      });
    }
  };
  return (
    <>
      <Navbar fixed="top" className="b-nav " variant="dark" expand="lg">
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Wrong Client crendentials</Modal.Title>
          </Modal.Header>
          <Modal.Body>Oops, Invalid Credentials, Or server offline</Modal.Body>
          <Modal.Footer>
            {/* <Button variant="secondary" onClick={handleClose}>
              Close
            </Button> */}
            <Button variant="primary" onClick={changeCrendential}>
              Change crendentials
            </Button>
          </Modal.Footer>
        </Modal>
        <div className="container b-nav">
          {/* <i className="fab fa-3x fa-fw fa-twitter c-white"></i> */}
          <i className="fas fa-3x fa-video c-white fa-twitter "></i>

          <Navbar.Brand href="#home">
            <Link className="nav-link c-white" to="/">
              App movies
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse
            id="basic-navbar-nav"
            className="justify-content-between"
          >
            <Nav className="mr-auto  text20">
              {/* <Link className="nav-link c-white" to="/">
            Home
          </Link> */}

              <Link className="nav-link c-white" to="/actors">
                Actors
              </Link>
              <Link className="nav-link c-white" to="/movies">
                Movies
              </Link>
            </Nav>

            <div className="row mr-0 ml-0 text20 c-white">
              {!localStorage.actorsToken ? (
                <div>
                  <div className="row m-0 mr-5">
                    <i className="fas fa-circle c-red mr-2 mt-2"></i>

                    <h3>Actors service</h3>
                  </div>
                  <div>
                    <button
                      onClick={connectActors}
                      className="btn btn-secondary"
                    >
                      Connect
                    </button>
                  </div>
                </div>
              ) : (
                <div>
                  <div className="row m-0 mr-5">
                    <i className="fas fa-circle c-green mr-2 mt-2"></i>

                    <h3>Actors service</h3>
                  </div>
                  <div>
                    <button
                      onClick={disconnectActors}
                      className="btn btn-secondary"
                    >
                      Disconnect
                    </button>
                  </div>
                </div>
              )}
              {/* activate Movies microservice */}
              {!localStorage.moviesToken ? (
                <div>
                  <div className="row m-0 mr-5">
                    <i className="fas fa-circle c-red mr-2 mt-2"></i>

                    <h3>Movies service</h3>
                  </div>
                  <div>
                    <button
                      onClick={connectMovies}
                      className="btn btn-secondary"
                    >
                      Connect
                    </button>
                  </div>
                </div>
              ) : (
                <div>
                  <div className="row m-0 mr-5">
                    <i className="fas fa-circle c-green mr-2 mt-2"></i>

                    <h3>Movies service</h3>
                  </div>
                  <div>
                    <button
                      onClick={disconnectMovies}
                      className="btn btn-secondary"
                    >
                      Disconnect
                    </button>
                  </div>
                </div>
              )}

              {/* <Link className="nav-link c-white" to="/signin"></Link> */}
              {/* <Link className="nav-link c-white" to="/signup"> */}
            </div>

            {/* <div className="username">
              <NavDropdown
                className="text-white"
                title={localStorage.name}
                id="nav-dropdown"
              >
                <div className="dp-user">
                  <NavDropdown.Item eventKey="4.1">
                    <Link to="/favoris">Favoris</Link>
                  </NavDropdown.Item>
                  <NavDropdown.Item
                    eventKey="4.2"
                    onClick={this.logOut.bind(this)}
                  >
                    Log out
                  </NavDropdown.Item>
                </div>
              </NavDropdown>
            </div> */}
          </Navbar.Collapse>
        </div>
      </Navbar>
    </>
  );
};

export default Test;
