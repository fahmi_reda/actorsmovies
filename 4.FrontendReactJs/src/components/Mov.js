import React, { useState, useEffect } from "react";
import {
  Spinner,
  Alert,
  Table,
  Modal,
  ModalBody,
  Button,
  Form,
} from "react-bootstrap";
import {
  getMovies,
  createMovie,
  deleteMovie,
  updateMovie,
} from "./MoviesFunctions";
import { mysql_date_to_js_date } from "./RandomFunctions";
import { Link } from "react-router-dom";
import Errors from "./services/Errors";
import { getActors } from "./ActorsFunctions";
import { actorsToken } from "./TokensFunctions";

const Movies = (props) => {
  const [errors, setErrors] = useState(Errors);

  const [moviesStatus, setMoviesStatus] = useState(false);
  const [moviesData, setMoviesData] = useState([]);
  const [name, setName] = useState("");
  const [refresh, setRefresh] = useState(false);
  const [movieId, setMovieId] = useState("");
  const [error, setError] = useState({ msg: "", status: false });
  const [actorsData, setActorsData] = useState([""]);
  const [datas, setDatas] = useState({
    actors1: "Joany Hintz",
    actors2: "Joany Hintz",
    actors3: "Joany Hintz",
  });

  const [responseStatus, setResponseStatus] = useState({
    msg: "",
    status: false,
  });

  //launch Create modal
  const [movieNewShow, setMovieNewShow] = useState(false);

  const handleClose = () => {
    setMovieNewShow(false);

    setResponseStatus({ msg: {}, status: false });
  };
  const handleShow = () => {
    errors.reset();
    console.log(errors);
    setResponseStatus({ msg: "", status: false });

    setMovieNewShow(true);
  };

  //launch delete modal
  const [deleteShow, setDeleteShow] = useState(false);
  const handleCloseDel = () => {
    setDeleteShow(false);
  };
  const handleShowDel = (e) => {
    setMovieId(e);
    setDeleteShow(true);
  };

  //launch update modal
  const [editShow, setEditShow] = useState(false);
  const handleCloseEdit = () => {
    setEditShow(false);
  };
  const handleShowEdit = (e) => {
    setResponseStatus({ msg: "", status: false });

    setName(e.name);
    setMovieId(e.id);
    setEditShow(true);
  };

  useEffect(() => {
    getMovies().then((res) => {
      setTimeout(() => {
        setMoviesStatus(true);
      }, 1000);

      if (!res) {
        setError({
          msg: "Server offline",
          status: true,
        });
      } else {
        if ((res && res.status == 401) || res.status == 500) {
          setError({
            msg: "Movies microservice down or you need authorisation",
            status: true,
          });
          console.log(error.msg);
        } else if (res && res.status == 200) {
          console.log(res);
          setMoviesData(res.data.data);
        }
      }
    });
    getActors().then((res) => {
      // setTimeout(() => {
      //   setActorsStatus(true);
      // }, 1000);
      if (!res) {
        setError({
          msg: "Server offline",
          status: true,
        });
      } else {
        if (res.status == 401 || res.status == 500) {
          // setError({
          //   msg: "Actors microservice down or you need authorisation",
          //   status: true,
          // });
        } else if (res && res.status == 200) {
          console.log(res);
          setActorsData(res.data.data);
        }
      }
    });
  }, [refresh]);
  const showMovie = () => {};
  const movieEditShow = () => {};
  // create function
  //onchange
  const createMovieOnChange = (e) => {
    setDatas({ ...datas, [e.target.id]: e.target.value });
    // console.log(datas)

    setName(e.target.value);
  };

  const UpdateMovieOnChange = (e) => {
    setName(e.target.value);
  };
  const createMovieSubmit = () => {
    console.log(datas);
    const newMovie = {
      name: datas.name,
      year: datas.year,
      actors: [datas.actors1, datas.actors2, datas.actors3],
    };
    console.log(newMovie);

    createMovie(newMovie).then((res) => {
      console.log(res);
      if (res.status == 422) {
        // setResponseStatus({ msg: "Required", status: true });
        // console.log(responseStatus.msg);
        errors.setErrors(res);
        console.log(errors.getKey("name"));
        setResponseStatus({ msg: errors.getKey("name"), status: true });

        // setResponseStatus({ status: true });
        console.log(errors);
      } else {
        handleClose();
        setRefresh(!refresh);
        setResponseStatus({ msg: "", status: false });
        setName("");
      }
      const actorsCredentials = {
        client_id: "actors",
        client_secret: "20nomalis21",
      };

      actorsToken(actorsCredentials).then((res) => {
        // props.history.push("/actors");
      });
    });

    // errors.reset();
  };

  //update function
  const updateMovieSubmit = () => {
    const newMovie = {
      name: name,
    };

    updateMovie(newMovie, movieId).then((res) => {
      console.log(res);
      if (res.status == 422) {
        setResponseStatus({ msg: res.data.error, status: true });
      } else {
        handleCloseEdit();
        setRefresh(!refresh);
        setResponseStatus({ msg: "", status: false });
        setName("");
      }
    });
  };
  // Delete function
  const deleteSubmit = (e) => {
    deleteMovie(e).then((res) => {
      console.log(res);
      handleCloseDel();
      setTimeout(() => {
        setRefresh(!refresh);
      }, 500);
    });
  };

  return (
    <>
      {moviesStatus ? (
        error.status ? (
          <Alert className="" variant="danger">
            Alert : {error.msg}
          </Alert>
        ) : (
          // Status OK and Authenticate
          <>
            <div className="row justify-content-between m-0">
              <h1>List of Movies</h1>
              <button
                onClick={handleShow}
                type="button"
                className="btn btn-outline-primary h-50"
              >
                New movie
              </button>
            </div>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Created_at</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {moviesData.map((item, i) => (
                  <>
                    <tr key={i}>
                      <td>{i + 1}</td>
                      <td>{item.name}</td>
                      <td>{mysql_date_to_js_date(item.created_at)}</td>
                      <td>
                        <Link className=" c-white" to={`/movies/${item.id}`}>
                          <Button
                            className="mr-2"
                            variant="primary"
                            onClick={showMovie}
                          >
                            View
                          </Button>
                        </Link>

                        {/* <Button
                            className="mr-2"
                            variant="success"
                            onClick={() => handleShowEdit(item)}
                          >
                            Edit
                        </Button> */}
                        <Button
                          className="mr-2"
                          variant="danger"
                          onClick={() => handleShowDel(item.id)}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  </>
                ))}
              </tbody>
            </Table>
          </>
        )
      ) : (
        <>
          <div className="row justify-content-center align-items-center py-5">
            <Spinner animation="border" variant="info" />
          </div>
        </>
      )}
      {/* Pop up Delete movie */}
      <Modal show={deleteShow} onHide={handleCloseDel}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Movie</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="Info">Are u sure to delete this movie ?</div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDel}>
            Cancel
          </Button>
          <Button variant="danger" onClick={() => deleteSubmit(movieId)}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>

      {/* Pop up New Acor */}
      <Modal show={movieNewShow} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New Movie</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <div className="form-group">
            <label>Name</label>
            <input
              type="text"
              className="form-control"
              name="name"
              id="name"
              placeholder="Enter Name Of Movie"
              // value={this.state.email}
              onChange={createMovieOnChange}
            />
            {errors.getKey("name") && (
              <div className="validation-error">{errors.getKey("name")}</div>
            )}
            <label>year</label>
            <input
              type="text"
              className="form-control"
              name="year"
              id="year"
              placeholder="Enter year"
              // value={this.state.email}
              onChange={createMovieOnChange}
            />
            {errors.getKey("year") && (
              <div className="validation-error">{errors.getKey("year")}</div>
            )}
            <Form.Label>Actors 1</Form.Label>
            <div>
              <select id="actors1" onChange={(e) => createMovieOnChange(e)}>
                {actorsData.map((item, i) => (
                  <option key={i} value={item.name}>
                    {item.name}
                  </option>
                ))}
              </select>
            </div>
            {/* second actor  */}
            <Form.Label>Actors 2</Form.Label>

            <div>
              <select
                id="actors2"
                name="actor[]"
                onChange={(e) => createMovieOnChange(e)}
              >
                {actorsData.map((item, i) => (
                  <option key={i} value={item.name}>
                    {item.name}
                  </option>
                ))}
              </select>
            </div>
            {/* third actor  */}
            <Form.Label>Actors 3</Form.Label>

            <div>
              <select id="actors3" onChange={(e) => createMovieOnChange(e)}>
                {actorsData.map((item, i) => (
                  <option key={i} value={item.name}>
                    {item.name}
                  </option>
                ))}
              </select>
            </div>

            {/* <Form.Control as="select">
                {actorsData.map((item, i) => (

                  <option>{item.name}</option>
                )

                )}

              </Form.Control> */}
            {/* {responseStatus.status && (
              <div className="validation-error">{errors.getKey("name")}</div>
            )   {/* {errors.getKey('name') && (
              <div className="validation-error">{errors.getKey("name")}</div>
            )}} */}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="success" onClick={createMovieSubmit}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>

      {/* Pop up Update Acor */}
      <Modal show={editShow} onHide={handleCloseEdit}>
        <Modal.Header closeButton>
          <Modal.Title>New Movie</Modal.Title>
        </Modal.Header>

        {responseStatus.status && (
          <Alert variant="danger">{responseStatus.msg}</Alert>
        )}
        <Modal.Body>
          <div className="form-group">
            <label>Name</label>

            <input
              type="text"
              className="form-control"
              name="name"
              placeholder="Enter Name Of Movie"
              value={name}
              onChange={UpdateMovieOnChange}
            />

            {/* {errors.getKey("errors")} */}
            {errors.errors && <div className="validation-error">{}</div>}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseEdit}>
            Cancel
          </Button>
          <Button variant="success" onClick={updateMovieSubmit}>
            Update
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default Movies;
