import React, { useState, useEffect } from "react";
import {
  Spinner,
  Alert,
  Table,
  Modal,
  ModalBody,
  Button,
} from "react-bootstrap";
import {
  getActors,
  createActor,
  deleteActor,
  updateActor,
} from "./ActorsFunctions";
import { mysql_date_to_js_date } from "./RandomFunctions";
import { Link } from "react-router-dom";
import Errors from "./services/Errors";

const Actors = (props) => {
  const [errors, setErrors] = useState(Errors);

  const [actorsStatus, setActorsStatus] = useState(false);
  const [actorsData, setActorsData] = useState([]);
  const [name, setName] = useState("");
  const [refresh, setRefresh] = useState(false);
  const [actorId, setActorId] = useState("");
  const [error, setError] = useState({ msg: "", status: false });
  const [d, setD] = useState("");

  const [responseStatus, setResponseStatus] = useState({
    msg: "",
    status: false,
  });

  //launch Create modal
  const [actorNewShow, setActorNewShow] = useState(false);

  const handleClose = () => {
    setActorNewShow(false);

    setResponseStatus({ msg: {}, status: false });
  };
  const handleShow = () => {
    errors.reset();
    console.log(errors);
    setResponseStatus({ msg: "", status: false });

    setActorNewShow(true);
  };

  //launch delete modal
  const [deleteShow, setDeleteShow] = useState(false);
  const handleCloseDel = () => {
    setDeleteShow(false);
  };
  const handleShowDel = (e) => {
    setActorId(e);
    setDeleteShow(true);
  };

  //launch update modal
  const [editShow, setEditShow] = useState(false);
  const handleCloseEdit = () => {
    setEditShow(false);
  };
  const handleShowEdit = (e) => {
    setResponseStatus({ msg: "", status: false });

    setName(e.name);
    setActorId(e.id);
    setEditShow(true);
  };

  useEffect(() => {
    getActors().then((res) => {
      setTimeout(() => {
        setActorsStatus(true);
      }, 1000);
      if (!res) {
        setError({
          msg: "Server offline",
          status: true,
        });
      } else {
        if ((res && res.status == 401) || res.status == 500) {
          setError({
            msg: "Actors microservice down or you need authorisation",
            status: true,
          });
          console.log(error.msg);
        } else if (res && res.status == 200) {
          setActorsData(res.data.data);
        }
      }
    });
  }, [refresh]);
  const showActor = () => {};
  const actorEditShow = () => {};
  // create function
  const createActorOnChange = (e) => {
    setName(e.target.value);
  };

  const UpdateActorOnChange = (e) => {
    setName(e.target.value);
  };
  const createActorSubmit = () => {
    const newActor = {
      name: name,
    };

    createActor(newActor).then((res) => {
      console.log(res);
      if (res.status == 422) {
        // setResponseStatus({ msg: "Required", status: true });
        // console.log(responseStatus.msg);
        errors.setErrors(res);
        console.log(errors.getKey("name"));
        setResponseStatus({ msg: errors.getKey("name"), status: true });

        // setResponseStatus({ status: true });
        console.log(errors);
      } else {
        handleClose();
        setRefresh(!refresh);
        setResponseStatus({ msg: "", status: false });
        setName("");
      }
    });
    // errors.reset();
  };

  //update function
  const updateActorSubmit = () => {
    const newActor = {
      name: name,
    };

    updateActor(newActor, actorId).then((res) => {
      console.log(res);
      if (res.status == 422) {
        setResponseStatus({ msg: res.data.error, status: true });
      } else {
        handleCloseEdit();
        setRefresh(!refresh);
        setResponseStatus({ msg: "", status: false });
        setName("");
      }
    });
  };
  // Delete function
  const deleteSubmit = (e) => {
    deleteActor(e).then((res) => {
      console.log(res);
      handleCloseDel();
      setTimeout(() => {
        setRefresh(!refresh);
      }, 500);
    });
  };

  return (
    <>
      {actorsStatus ? (
        error.status ? (
          <Alert className="" variant="danger">
            Alert : {error.msg}
          </Alert>
        ) : (
          // Status OK and Authenticate
          <>
            <div className="row justify-content-between m-0">
              <h1>List of Actors</h1>
              <button
                onClick={handleShow}
                type="button"
                className="btn btn-outline-primary h-50"
              >
                New actor
              </button>
            </div>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Created_at</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {actorsData.map((item, i) => (
                  <>
                    <tr key={i}>
                      <td>{i + 1}</td>
                      <td>{item.name}</td>
                      <td>{mysql_date_to_js_date(item.created_at)}</td>
                      <td>
                        <Link className=" c-white" to={`/actors/${item.id}`}>
                          <Button
                            className="mr-2"
                            variant="primary"
                            onClick={showActor}
                          >
                            View
                          </Button>
                        </Link>

                        <Button
                          className="mr-2"
                          variant="success"
                          onClick={() => handleShowEdit(item)}
                        >
                          Edit
                        </Button>
                        <Button
                          className="mr-2"
                          variant="danger"
                          onClick={() => handleShowDel(item.id)}
                        >
                          Delete
                        </Button>
                      </td>
                    </tr>
                  </>
                ))}
              </tbody>
            </Table>
          </>
        )
      ) : (
        <>
          <div className="row justify-content-center align-items-center py-5">
            <Spinner animation="border" variant="info" />
          </div>
        </>
      )}
      {/* Pop up Delete actor */}
      <Modal show={deleteShow} onHide={handleCloseDel}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Actor</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="Info">Are u sure to delete this actor ?</div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDel}>
            Cancel
          </Button>
          <Button variant="danger" onClick={() => deleteSubmit(actorId)}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>

      {/* Pop up New Acor */}
      <Modal show={actorNewShow} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New Actor</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <div className="form-group">
            <label>Name</label>
            <input
              type="text"
              className="form-control"
              name="name"
              placeholder="Enter Name Of Actor"
              // value={this.state.email}
              onChange={createActorOnChange}
            />
            {/* {responseStatus.status && (
              <div className="validation-error">{errors.getKey("name")}</div>
            )} */}
            {errors.getKey("name") && (
              <div className="validation-error">{errors.getKey("name")}</div>
            )}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cancel
          </Button>
          <Button variant="success" onClick={createActorSubmit}>
            Confirm
          </Button>
        </Modal.Footer>
      </Modal>

      {/* Pop up Update Acor */}
      <Modal show={editShow} onHide={handleCloseEdit}>
        <Modal.Header closeButton>
          <Modal.Title>New Actor</Modal.Title>
        </Modal.Header>

        {responseStatus.status && (
          <Alert variant="danger">{responseStatus.msg}</Alert>
        )}
        <Modal.Body>
          <div className="form-group">
            <label>Name</label>

            <input
              type="text"
              className="form-control"
              name="name"
              placeholder="Enter Name Of Actor"
              value={name}
              onChange={UpdateActorOnChange}
            />

            {/* {errors.getKey("errors")} */}
            {errors.errors && <div className="validation-error">{}</div>}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseEdit}>
            Cancel
          </Button>
          <Button variant="success" onClick={updateActorSubmit}>
            Update
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default Actors;
