import axios from "axios";
const gatewayUrl = process.env.REACT_APP_GATEWAY_URL;

// get all movies
export const getMovies = () => {
  return axios
    .get(`${gatewayUrl}/films`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.moviesToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

// get single movie
export const getSingleMovie = (id) => {
  return axios
    .get(`${gatewayUrl}/film/details/${id}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.moviesToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

// get films by movie id
export const getFilmsByMovie = (id) => {
  return axios
    .get(`${gatewayUrl}/acteur/${id}/films`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.moviesToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

// create new movie
export const createMovie = (newMovie) => {
  console.log(newMovie)
  return axios
    .post(`${gatewayUrl}/film/ajouter`, newMovie, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.moviesToken}`,
      },
    })
    .then((res) => {
      return res;
      console.log(res)
    })
    .catch((err) => {
      console.log(err)

      return err.response;
    });
};
// update new movie
export const updateMovie = (newMovie, id) => {
  // console.log(e);
  return axios
    .put(`${gatewayUrl}/film/modifier/${id}`, newMovie, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.moviesToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
// get single movie
export const deleteMovie = (movieId) => {
  return axios
    .delete(`${gatewayUrl}/film/supprimer/${movieId}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.moviesToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

export const moviesToken = (moviesCredentials) => {
  //api twitter feed
  console.log(moviesCredentials);
  return axios
    .post(`${gatewayUrl}/film/oauth/token`, moviesCredentials, {
      headers: { "Content-Type": "application/json" },
    })
    .then((res) => {
      console.log(res);
      localStorage.setItem("moviesToken", res.data.access_token);

      // return res;
    })
    .catch((err) => {
      return err.response;
      console.log(err);
    });
};

//////////////////////////////////////////////////////////////////
// export const login = user => {
//   return axios
//     .post(
//       `${api}/api/login`,
//       {
//         email: user.email,
//         password: user.password
//       },
//       {
//         headers: { "Content-Type": "application/json" }
//       }
//     )
//     .then(res => {
//       localStorage.setItem("usertoken", res.data.access_token);
//       localStorage.setItem("name", res.data.user.name);
//       console.log(res);
//     })
//     .catch(err => {
//       console.log(err);
//     });
// };
// //////////////////////////////////////////////////////////////////////

// export const getProfile = newUser => {
//   return axios
//     .get(`${api}/api/user`, {
//       headers: { Authorization: `Bearer ${localStorage.usertoken}` }
//     })
//     .then(res => {
//       console.log(res);
//       return res.data;
//     })
//     .catch(err => {
//       console.log(err);
//     });
// };
