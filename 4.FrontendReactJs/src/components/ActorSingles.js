import React, { useState, useEffect } from "react";
import { getSingleActor, getFilmsByActor } from "./ActorsFunctions";
import { Card, Button, Badge, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import { mysql_date_to_js_date } from "./RandomFunctions";

const ActorSingle = (props) => {
  const id = props.match.params.id;
  const [refresh, setRefresh] = useState(false);
  const [data, setData] = useState("");
  const [filmsByActor, setFilmsByActor] = useState([]);
  const [errorMsg, setErrorMsg] = useState({ msg: "", status: "" });

  useEffect(() => {
    setErrorMsg({ msg: "", status: "" });
    getSingleActor(id).then((res) => {
      setTimeout(() => {
        // setActorsStatus(true);
      }, 1000);
      if (res.status == 401 || res.status == 500) {
        // setError({ msg: res.data.error, status: true });
        // console.log(error.msg);
      } else if (res && res.status == 200) {
        console.log(res);
        setData(res.data.data);
      }
    });
    getFilmsByActor(id).then((res) => {
      setTimeout(() => {
        // setActorsStatus(true);
      }, 1000);
      if (res.status == 401 || res.status == 500) {
        console.log(res);
        setErrorMsg({
          msg: "Movies microservice offline",
          status: "text-danger",
        });
      } else if (res.status == 200) {
        setFilmsByActor(res.data.data);
        console.log("res 200");
        if (Array.isArray(res.data.data) && !res.data.data.length) {
          setErrorMsg({ msg: "No Movies for this Actor", status: "text-info" });
        }
        console.log(res);
      }
    });
  }, [refresh]);

  return (
    <Card>
      <Card.Header className="text-success text-capitalize ">
        <h2>{data.name}</h2>
      </Card.Header>
      <Card.Body className="mb-5">
        <Card.Title> Filmographie</Card.Title>
        {errorMsg && <div className={errorMsg.status}>{errorMsg.msg}</div>}
        {filmsByActor.map((item, i) => (
          <>
            <Row key={i} className="m-2">
              <Col xs={6} md={4}>
                <Badge className="text15" variant="info">
                  {item.name}
                </Badge>
              </Col>
              <Col sm={1}>
                <Badge variant="secondary">{item.year}</Badge>
              </Col>
            </Row>
          </>
        ))}

        {/* <Card.Text className="text-muted">Date: {data.created_at}</Card.Text> */}
      </Card.Body>
      <Card.Footer className="text-capitalize ">
        <Link className=" c-white" to={`/actors`}>
          <Button variant="primary">Back</Button>
        </Link>
      </Card.Footer>
    </Card>
  );
};

export default ActorSingle;
