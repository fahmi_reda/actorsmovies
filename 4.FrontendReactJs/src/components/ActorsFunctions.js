import axios from "axios";
const gatewayUrl = process.env.REACT_APP_GATEWAY_URL;

// get all actors
export const getActors = () => {
  return axios
    .get(`${gatewayUrl}/acteurs`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.actorsToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      console.log(err.response);

      return err.response;
    });
};

// get single actor
export const getSingleActor = (id) => {
  return axios
    .get(`${gatewayUrl}/acteur/details/${id}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.actorsToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

// get films by actor id
export const getFilmsByActor = (id) => {
  return axios
    .get(`${gatewayUrl}/acteur/${id}/films`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.moviesToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

// create new actor
export const createActor = (newActor) => {
  return axios
    .post(`${gatewayUrl}/acteur/ajouter`, newActor, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.actorsToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
// update new actor
export const updateActor = (newActor, id) => {
  // console.log(e);
  return axios
    .put(`${gatewayUrl}/acteur/modifier/${id}`, newActor, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.actorsToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};
// get single actor
export const deleteActor = (actorId) => {
  return axios
    .delete(`${gatewayUrl}/acteur/supprimer/${actorId}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.actorsToken}`,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response;
    });
};

export const moviesToken = (moviesCredentials) => {
  //api twitter feed
  console.log(moviesCredentials);
  return axios
    .post(`${gatewayUrl}/film/oauth/token`, moviesCredentials, {
      headers: { "Content-Type": "application/json" },
    })
    .then((res) => {
      console.log(res);
      localStorage.setItem("moviesToken", res.data.access_token);

      // return res;
    })
    .catch((err) => {
      return err.response;
      console.log(err);
    });
};

//////////////////////////////////////////////////////////////////
// export const login = user => {
//   return axios
//     .post(
//       `${api}/api/login`,
//       {
//         email: user.email,
//         password: user.password
//       },
//       {
//         headers: { "Content-Type": "application/json" }
//       }
//     )
//     .then(res => {
//       localStorage.setItem("usertoken", res.data.access_token);
//       localStorage.setItem("name", res.data.user.name);
//       console.log(res);
//     })
//     .catch(err => {
//       console.log(err);
//     });
// };
// //////////////////////////////////////////////////////////////////////

// export const getProfile = newUser => {
//   return axios
//     .get(`${api}/api/user`, {
//       headers: { Authorization: `Bearer ${localStorage.usertoken}` }
//     })
//     .then(res => {
//       console.log(res);
//       return res.data;
//     })
//     .catch(err => {
//       console.log(err);
//     });
// };
