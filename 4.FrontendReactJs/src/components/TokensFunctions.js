import axios from "axios";
const gatewayUrl = process.env.REACT_APP_GATEWAY_URL;

export const actorsToken = (actorsCredentials) => {
  return axios
    .post(`${gatewayUrl}/acteur/oauth/token`, actorsCredentials, {
      headers: { "Content-Type": "application/json" },
    })
    .then((res) => {
      console.log(res);

      localStorage.setItem("actorsToken", res.data.access_token);

      return res;
    })
    .catch((err) => {
      return err.response;
      console.log(err);
    });
};
export const moviesToken = (moviesCredentials) => {
  //api twitter feed
  // console.log(moviesCredentials);
  return axios
    .post(`${gatewayUrl}/film/oauth/token`, moviesCredentials, {
      headers: { "Content-Type": "application/json" },
    })
    .then((res) => {
      console.log(res);
      localStorage.setItem("moviesToken", res.data.access_token);

      return res;
    })
    .catch((err) => {
      return err.response;
      console.log(err);
    });
};

//////////////////////////////////////////////////////////////////
// export const login = user => {
//   return axios
//     .post(
//       `${api}/api/login`,
//       {
//         email: user.email,
//         password: user.password
//       },
//       {
//         headers: { "Content-Type": "application/json" }
//       }
//     )
//     .then(res => {
//       localStorage.setItem("usertoken", res.data.access_token);
//       localStorage.setItem("name", res.data.user.name);
//       console.log(res);
//     })
//     .catch(err => {
//       console.log(err);
//     });
// };
// //////////////////////////////////////////////////////////////////////

// export const getProfile = newUser => {
//   return axios
//     .get(`${api}/api/user`, {
//       headers: { Authorization: `Bearer ${localStorage.usertoken}` }
//     })
//     .then(res => {
//       console.log(res);
//       return res.data;
//     })
//     .catch(err => {
//       console.log(err);
//     });
// };
