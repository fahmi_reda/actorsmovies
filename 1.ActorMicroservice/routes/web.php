<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * Get Token Actor microservice
 */
$router->post('/actors/token', 'ActorController@actorToken');

/**
 * Routes Actors microservice
 */
$router->group(['middleware' => 'client.credentials'], function () use ($router) {

    $router->get('/actors/all', 'ActorController@index');
    $router->post('/actor/create', 'ActorController@store');
    $router->get('/actor/read/{id}', 'ActorController@show');
    $router->put('/actor/update/{id}', 'ActorController@update');
    $router->patch('/actor/update/{id}', 'ActorController@update');
    $router->delete('/actor/delete/{id}', 'ActorController@destroy');

    $router->post('/actor/find', 'ActorController@findActor');
});
