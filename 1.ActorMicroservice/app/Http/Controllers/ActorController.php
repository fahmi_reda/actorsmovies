<?php

namespace App\Http\Controllers;

use App\Models\Actor;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;
use GuzzleHttp\client;

// use Laravel\Passport\PersonalAccessTokenFactory;
// use Laravel\Passport\ClientRepository;
// use Dusterio\LumenPassport\LumenPassport;

class ActorController extends Controller
{
    //trait for custom response Api call
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    { }


    public function actorToken(Request $request)
    {
        $client = Passport::client()->where('name', $request->client_id)->first();
        if (!isset($client)) {
            return $this->errorResponse('Client not found', Response::HTTP_NOT_FOUND);
        }
        return $this->successResponse($client->id);
    }

    public function index()
    {

        $actors = Actor::orderBy('created_at', 'desc')->get();
        return $this->successResponse($actors);
    }

    public function show($id)
    {
        $actor = Actor::findOrFail($id);
        return $this->successResponse($actor);
    }
    public function findActor(Request $request)
    {
      
        // $actors = unserialize($request->actors);
        // return $request->actors;
        // return serialize($request->actors);
        // return $request->all();
        $actorsData = [];
        $actorsNotFound = [];
        $rules = [
            'actors' => 'required|array',

        ];
        $this->validate($request, $rules);

        foreach ($request->actors as $key => $actor) {


            $data = Actor::where('slug', 'like',  Str::slug($actor, '-'))->first();

            if ($data) {

                array_push($actorsData, ['id' => $data->id, 'name' => $data->name]);
            } else {
                array_push($actorsNotFound, $actor);
            }
        }
        if ($actorsNotFound) {
            // return $this->successResponse($actorsNotFound, Response::HTTP_NOT_FOUND);
            return response()->json(['actorsNotFound' => $actorsNotFound, 'code' => 404], Response::HTTP_ACCEPTED);

            // return $this->errorResponse(["actorNotFound" => $actorsNotFound], Response::HTTP_NOT_FOUND);
        } else {
            return $this->successResponse($actorsData);
        }
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',

        ];
        $this->validate($request, $rules);
        $actor = Actor::create([
            'name' => $request->name,
            'slug' => Str::slug($request->name, '-')
        ]);
        return $this->successResponse($actor, Response::HTTP_CREATED);
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $rules = [
            'name' => 'max:255',

        ];
        $this->validate($request, $rules);
        $actor = Actor::findOrFail($id);
        $actor->fill($request->all());

        //check if atleast 1 data changed in case we have more input.
        if ($actor->isClean()) {
            return $this->errorResponse('atleast one value must change', Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $actor->slug = Str::slug($request->name);

        $actor->save();
        return $this->successResponse($actor);
    }
    public function destroy($id)
    {
        $actor = Actor::findOrFail($id);
        $actor->delete();
        return $this->successResponse($actor);
    }
}
