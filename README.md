# Microservices (Actors-movies)

microservices App for movies
![alt text](image.jpg)

## Getting started

1. Clone project `git clone https://fahmi_reda@bitbucket.org/fahmi_reda/actorsmovies.git`
2. Go to the folder's `cd nameOfMicroservice`
3. Install composer `composer install`
4. Import Database `Database-Actors-Movies-Gateway.sql`

5. Launch server's (Actors/Movies/Gateway) in each folder `launch-{nameMicro}.bat`
6. Test endpoint with postman `check postman collection json given`
7. go folder frontend and `npm install`
8. Start server `npm start`
